module Extra.Data.List.Relation.Binary.Permutation.Propositional where

open import Data.List
open import Data.List.Membership.Propositional using (_∈_)
import Data.List.Properties as List
open import Data.List.Relation.Unary.All as All using (All; _∷_; [])
open import Data.List.Relation.Unary.All.Properties as All using (++⁺)
open import Data.List.Relation.Unary.Any as Any using (Any; here; there)

open import Data.List.Relation.Binary.Permutation.Propositional public
open import Data.List.Relation.Binary.Permutation.Propositional.Properties public
