module Extra.Data.Product.Nary.NonDependent where

open import Data.Fin
open import Data.Product.Nary.NonDependent
open import Data.Unit.Polymorphic.Base
open import Extra.Function.Nary.NonDependent.Base
open import Foundation hiding (map′; ⊤; _≅_)
open import Function.Nary.NonDependent as Fun hiding (mapₙ)
open import Numbers.Nat hiding (_⊔_)
open import Relation.Nary

private variable
  a : Level
  A : Set a
  n : ℕ
  ls ls' ls″ : Levels n

private
  Prod : ∀{n ls} → Sets n ls → Set (⨆ n ls)
Prod {n} = Product⊤ n

------------------------------------------------------------------------
-- Basic operations

llength : Levels n → ℕ
llength {n = n} _ = n

slength : Sets n ls → ℕ
slength {n = n} _ = n

length : {As : Sets n ls} → Prod As → ℕ
length {n = n} _ = n

llookup : Levels n → Fin n → Level
llookup (l , _)  zero    = l
llookup (_ , ls) (suc x) = llookup ls x

slookup : Sets n ls → (x : Fin n) → Set (llookup ls x)
slookup (A , _)  zero = A
slookup (_ , As) (suc x) = slookup As x

lookup : {As : Sets n ls} → Prod As → (x : Fin n) → slookup As x
lookup (a , _)  zero = a
lookup (_ , as) (suc x) = lookup as x

-- TODO: implement 
-- insert :  n → Fin (suc n) → A → Vec A (suc n)
-- remove : ∀ {n} → Vec A (suc n) → Fin (suc n) → Vec A n
-- updateAt : ∀ {n} → Fin n → (A → A) → Vec A n → Vec A n

------------------------------------------------------------------------
-- Operations for transforming nary products

-- zipping

lzipWith : (f : (l l' : Level) → Level)(ls ls' : Levels n) → Levels n
lzipWith {zero} f ls       ls'        = _
lzipWith {n +1} f (l , ls) (l' , ls') = f l l' , lzipWith f ls ls'

szipWith :
  {flvl : (l l' : Level) → Level}
  (F : ∀{a}{b} → Set a → Set b → Set (flvl a b))
  (As : Sets n ls)(Bs : Sets n ls') → Sets n (lzipWith flvl ls ls')
szipWith {n = zero} F As       Bs      = _
szipWith {n = n +1} F (A , As)(B , Bs) = F A B , szipWith F As Bs

zipWith :
  {flvl : (l l' : Level) → Level}
  {F : ∀{a}{b} → Set a → Set b → Set (flvl a b)}
  {As : Sets n ls}{Bs : Sets n ls'} →
  (f : ∀{a}{b}{A : Set a}{B : Set b} → A → B → F A B) →
  Prod As → Prod Bs → Prod (szipWith F As Bs)
zipWith {zero} f _       _        = _
zipWith {n +1} f (a , as)(b , bs) = f a b , zipWith f as bs

-- zip apply

lFunFroms : (ls ls' : Levels n) → Levels n
lFunFroms = lzipWith _⊔_

FunsNonDep : (As : Sets n ls)(Bs : Sets n ls') →
  Sets n (lzipWith _⊔_ ls ls')
FunsNonDep {zero} _        _        = _
FunsNonDep {n +1} (A , As) (B , Bs) = (A → B) , FunsNonDep As Bs

zipapplynondep : {As : Sets n ls}{Bs : Sets n ls'} →
  Prod (FunsNonDep As Bs) → Prod As → Prod Bs
zipapplynondep {zero} _        _        = _
zipapplynondep {n +1} (f , fs) (a , as) = f a , zipapplynondep fs as

-- toFunsNonDep :
--   {Fs : Sets n (lzipWith _⊔_ ls ls')}{As : Sets n ls}{Bs : Sets n ls'} →
--   Prod (szipWith (λ F A→B → F ≅ A→B) Fs (szipWith Morphism As Bs)) →
--   Prod Fs → Prod (FunsNonDep As Bs)
-- toFunsNonDep {zero} eqs          fs       = _
-- toFunsNonDep {n +1} (refl , eqs) (f , fs) = f , toFunsNonDep eqs fs

FunsDep : (As : Sets n ls)(Bs : Prod (FunsNonDep As (Levels-toSets ls'))) →
  Sets n (lzipWith _⊔_ ls ls')
FunsDep {zero} _        _        = _
FunsDep {n +1} (A , As) (B , Bs) = ((a : A) → B a) , FunsDep As Bs

zipapplydep :
  {As : Sets n ls}{Bs : Prod (FunsNonDep As (Levels-toSets ls'))} →
  Prod (FunsDep As Bs) → (as : Prod As) → Prod (Product⊤-toSets n (zipapplynondep Bs as))
zipapplydep {zero} _        _        = _
zipapplydep {n +1} (f , fs) (a , as) = f a , zipapplydep fs as

-- indexed zipping

izipwith : ∀{i}(I : Set i)
  {flvlA flvlB flvlR : I → Level}
  (fAs : (i : I) → Set (flvlA i))
  (fBs : (i : I) → Set (flvlB i))
  (fRs : (i : I) → Set (flvlR i))
  (f : {i : I} → fAs i → fBs i → fRs i)
  {n}(is : Prod (sreplicate n I)) →
  Prod (simap fAs is) → Prod (simap fBs is) → Prod (simap fRs is)
izipwith I fAs fBs fRs f {zero} is _ _ = _
izipwith I fAs fBs fRs f {n +1} (_ , is)(a , as)(b , bs) =
  f a b , izipwith I fAs fBs fRs f is as bs

izipwith-param : ∀{i}(I : Set i)
  {flvlA flvlB flvlR : I → Level}
  (fAs : (i : I) → Set (flvlA i))
  (fBs : (i : I) → Set (flvlB i))
  (fRs : (i : I) → Set (flvlR i))
  (f : {i : I} → fAs i → fBs i → fRs i)
  {n}(is : Prod (sreplicate n I))
  {As : Sets n (limap flvlA is)}
  {Bs : Sets n (limap flvlB is)}
  {Rs : Sets n (limap flvlR is)} →
  simap fAs is ≡ As → simap fBs is ≡ Bs → simap fRs is ≡ Rs →
  Prod As → Prod Bs → Prod Rs
izipwith-param I fAs fBs fRs f is refl refl refl =
  izipwith I fAs fBs fRs f is

map′ : ∀ m {ls}{As : Sets m ls}{ls'}{Bs : Sets m ls'} →
  Prod (szipWith Morphism As Bs) → Prod As → Prod Bs
map′ zero _ _ = _
map′ (m +1) (f , fs)(a , as) = f a , map′ m fs as

map : ∀ m {ls}{As : Sets m ls}{ls'}{Bs : Sets m ls'} →
  szipWith Morphism As Bs ⇉ (Prod As → Prod Bs)
map m = curry⊤ₙ m (map′ m)

mapₙ′ : ∀ m {ls}{As : Sets m ls} n {ls'}{Bs : Sets n ls'} →
  Prod (smap _ (As ⇉_) n Bs) → Prod As → Prod Bs
mapₙ′ m 0 fs as = _
mapₙ′ m (n +1) (f , fs) as = uncurry⊤ₙ m f as , mapₙ′ m n fs as

mapₙ : ∀ m {ls}{As : Sets m ls} n {ls'}{Bs : Sets n ls'} →
  smap _ (As ⇉_) n Bs ⇉ (Prod As → Prod Bs)
mapₙ m n = curry⊤ₙ n (mapₙ′ m n)

replicate⊤ : ∀ n (a : A) → Prod (sreplicate n A)
replicate⊤ zero a = _
replicate⊤ (n +1) a = a , replicate⊤ n a

------------------------------------------------------------------------
-- Operations for reducing nary products

foldr-hom : ∀ {a b} {A : Set a} (B : ℕ → Set b) →
        (∀ {n} → A → B n → B (n +1)) →
        B zero →
        ∀ m → Prod (sreplicate m A) → B m
foldr-hom b _⊕_ n 0     _        = n
foldr-hom b _⊕_ n (m +1)(x , xs) = x ⊕ foldr-hom b _⊕_ n m xs

foldr₁-hom : (A → A → A) → ∀ m → Prod (sreplicate (m +1) A) → A
foldr₁-hom _⊕_ 0      (x , _)      = x
foldr₁-hom _⊕_ (m +1) (x , y , ys) = x ⊕ foldr₁-hom _⊕_ m (y , ys)


