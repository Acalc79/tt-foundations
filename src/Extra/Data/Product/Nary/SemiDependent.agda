module Extra.Data.Product.Nary.SemiDependent where

open import Data.Product.Nary.NonDependent
open import Extra.Function.Nary.SemiDependent.Base
open import Foundation
open import Function.Nary.NonDependent
open import Numbers.Nat

scurry⊤ₙ′ : ∀ n {ls} {As : Sets n ls} {r} {B : Product⊤ n As → Set r} →
            ((as : Product⊤ n As) → B as) → As ⇶′ B
scurry⊤ₙ′ 0 f = f _
scurry⊤ₙ′ (n +1) f = scurry⊤ₙ′ n ∘ curry f

scurry⊤ₙω : ∀ n {ls} {As : Sets n ls}{B : Product⊤ n As → Setω} →
            ((as : Product⊤ n As) → B as) → As ⇶ω B
scurry⊤ₙω 0 f = f _
scurry⊤ₙω (n +1) f a = scurry⊤ₙω n λ as → f (a , as)

unscurry⊤ₙ′ : ∀ n {ls} {As : Sets n ls} {r} {B : Product⊤ n As → Set r} →
              As ⇶′ B → (as : Product⊤ n As) → B as
unscurry⊤ₙ′ zero    f = const f
unscurry⊤ₙ′ (n +1) f = uncurry (unscurry⊤ₙ′ n ∘ f)
