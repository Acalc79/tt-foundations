module Extra.Data.List where

open import Data.List hiding (_─_)
open import Data.List.Membership.Propositional using (_∈_)
open import Data.List.Properties as List
open import Data.List.Relation.Binary.Permutation.Propositional
  using (_↭_; ↭-sym; module PermutationReasoning) renaming (module _↭_ to ↭)
open import Data.List.Relation.Binary.Permutation.Propositional.Properties
open import Data.List.Relation.Unary.All as All using (All; _∷_; [])
open import Data.List.Relation.Unary.All.Properties as All using (++⁺)
open import Data.List.Relation.Unary.Any as Any using (Any; here; there; _─_)
open import Data.Sum as Sum using (_⊎_; inj₁; inj₂)
open import Function using (_$_)
open import Level using (Level)
open import Relation.Unary using (Pred)
open import Relation.Binary.PropositionalEquality using (_≡_; refl)

private
  variable
    a p : Level
    A : Set a

------------------------------------------------------------------------
-- foldr

-- Interaction with predicates

module _ {P : Pred A p} {f : A → A → A} where -- 
  foldr-forcesᵒ : (∀ x y → P (f x y) → P x ⊎ P y) →
                  ∀ e xs → P (foldr f e xs) → Any P xs ⊎ P e
  foldr-forcesᵒ forces _ [] Pe = inj₂ Pe
  foldr-forcesᵒ forces e (x ∷ xs) Pfold with forces _ _ Pfold
  ... | inj₁ Px = inj₁ (here Px)
  ... | inj₂ Pfold = Sum.map₁ there $ foldr-forcesᵒ forces e xs Pfold

------------------------------------------------------------------------
-- _↭_ and _─_

∈↭hd : ∀{x : A}{xs}(x∈xs : x ∈ xs) → xs ↭ x ∷ (xs Any.─ x∈xs)
∈↭hd (here refl) = ↭.refl
∈↭hd {x = x}{y ∷ xs}(there x∈xs) = ↭.trans (↭.prep y $ ∈↭hd x∈xs) (↭.swap y x ↭.refl)

─⁺ : ∀{x y : A}{xs ys}(x≡y : x ≡ y)(x∈xs : x ∈ xs)(y∈ys : y ∈ ys) →
  xs ↭ ys → xs ─ x∈xs ↭ ys ─ y∈ys
─⁺ {x = x}{xs = xs}{ys} refl x∈xs x∈ys xs↭ys = drop-∷ $ begin
  x ∷ (xs ─ x∈xs) ↭˘⟨ ∈↭hd x∈xs ⟩
  xs ↭⟨ xs↭ys ⟩
  ys ↭⟨ ∈↭hd x∈ys ⟩
  x ∷ (ys ─ x∈ys) ∎
  where open PermutationReasoning
