module Extra.Function.Nary.SemiDependent.Base where

open import Data.Product.Nary.NonDependent
open import Foundation
open import Function.Nary.NonDependent
open import Numbers.Nat hiding (_⊔_)
open import Relation.Nary

private
  variable
    a b c r : Level
    A : Set a
    B : Set b
    C : Set c

SemiDepArrows :
  ∀ n {r ls}(As : Sets n ls) → As ⇉ Set r → Set (r ⊔ (⨆ n ls))
SemiDepArrows 0 _ b = b
SemiDepArrows (n +1)(A , As) F = (a : A) → SemiDepArrows n As (F a)

SemiDepArrows′ :
  ∀ n {r ls}(As : Sets n ls)
  (F : Product⊤ n As → Set r) → Set (r ⊔ (⨆ n ls))
SemiDepArrows′ 0 _ F = F _
SemiDepArrows′ (n +1)(A , As) F =
  (a : A) → SemiDepArrows′ n As (F ∘ (a ,_))

SemiDepArrowsω :
  ∀ n {ls}(As : Sets n ls)
  (F : Product⊤ n As → Setω) → Setω
SemiDepArrowsω 0 _ F = F _
SemiDepArrowsω (n +1)(A , As) F =
  (a : A) → SemiDepArrowsω n As λ as → F (a , as)

infix 0 _⇶_ _⇶′_ _⇶ω_
_⇶_ : ∀ {n ls r}(as : Sets n ls) → as ⇉ Set r → Set (r ⊔ (⨆ n ls))
_⇶_ = SemiDepArrows _

_⇶′_ : ∀{n r ls}(As : Sets n ls) →
  (Product⊤ n As → Set r) → Set (r ⊔ (⨆ n ls))
_⇶′_ = SemiDepArrows′ _

_⇶ω_ : ∀{n ls}(As : Sets n ls) →
  (Product⊤ n As → Setω) → Setω
_⇶ω_ = SemiDepArrowsω _

sdmap : ∀ n {ls}{As : Sets n ls}{F : As ⇉ Set a}{G : As ⇉ Set b} →
  As ⇶ liftₙ 2 n Morphism F G → As ⇶ F → As ⇶ G
sdmap 0 f v = f v
sdmap (n +1) f g a = sdmap n (f a) (g a)

sdmap′ :
  ∀ n {ls}{As : Sets n ls}
  {F : Product⊤ n As → Set a}{G : Product⊤ n As → Set b} →
  (∀ as → F as → G as) → As ⇶′ F → As ⇶′ G
sdmap′ 0 morph v = morph _ v
sdmap′ (n +1) {As = A , As} morph f a = sdmap′ n (morph ∘ (a ,_)) (f a)
