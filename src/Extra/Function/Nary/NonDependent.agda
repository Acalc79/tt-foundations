module Extra.Function.Nary.NonDependent where

open import Data.Product.Nary.NonDependent
open import Extra.Data.Product.Nary.NonDependent as Prod
import Extra.Function.Nary.NonDependent.Base as NonDep
open import Foundation
open import Function.Nary.NonDependent
open import Numbers.Nat hiding (_⊔_)

open NonDep public

private variable
  a b l r : Level
  A : Set a
  B : Set b
  n : ℕ

Opₙ : (n : ℕ)(A : Set a) → Set (a ⊔ ⨆ n (lreplicate n a))
Opₙ n A = sreplicate n A ⇉ A

Opₙ-toArrows : ∀{A : Set a} n → Opₙ n A → sreplicate n A ⇉ A
Opₙ-toArrows _ = id

Arrows-toOpₙ : ∀{A : Set a} n → sreplicate n A ⇉ A → Opₙ n A
Arrows-toOpₙ _ = id

curryOp⊤ : ∀ n {A : Set a} → (Product⊤ n (sreplicate n A) → A) → Opₙ n A
curryOp⊤ n = curry⊤ₙ n

uncurryOp⊤ : ∀ n {A : Set a} → Opₙ n A → Product⊤ n (sreplicate n A) → A
uncurryOp⊤ n = uncurry⊤ₙ n

-- not defined using _⇉_ to make levels compute to a
{-
Opₙ : (n : ℕ)(A : Set a) → Set a
Opₙ zero A = A
Opₙ (n +1) A = A → Opₙ n A

Opₙ-toArrows : ∀{A : Set a} n → Opₙ n A → sreplicate n A ⇉ A
Opₙ-toArrows zero = id
Opₙ-toArrows (n +1) = Opₙ-toArrows n ∘_

Arrows-toOpₙ : ∀{A : Set a} n → sreplicate n A ⇉ A → Opₙ n A
Arrows-toOpₙ zero = id
Arrows-toOpₙ (n +1) = Arrows-toOpₙ n ∘_

curryOp⊤ : ∀ n {A : Set a} → (Product⊤ n (sreplicate n A) → A) → Opₙ n A
curryOp⊤ n = Arrows-toOpₙ n ∘ curry⊤ₙ n

uncurryOp⊤ : ∀ n {A : Set a} → Opₙ n A → Product⊤ n (sreplicate n A) → A
uncurryOp⊤ n = uncurry⊤ₙ n ∘ Opₙ-toArrows n
-}


Product⊤-toLevels : ∀ n → Product⊤ n (sreplicate n Level) → Levels n
Product⊤-toLevels 0 _ = _
Product⊤-toLevels (n +1) (l , ls) = l , Product⊤-toLevels n ls

rep-tolmap : ∀ n
  --------------------------------------------------
  (flvl : A → Level)
  (p : Product⊤ n (sreplicate n A)) → Levels n
rep-tolmap zero flvl p = _
rep-tolmap (n +1) flvl (a , as) = flvl a , rep-tolmap n flvl as

rep-tosmap : ∀ n
  --------------------------------------------------
  {flvl : A → Level}
  (f : (x : A) → Set (flvl x))
  (p : Product⊤ n (sreplicate n A)) → Sets n (rep-tolmap n flvl p)
rep-tosmap zero f p = _
rep-tosmap (n +1) f (a , as) = f a , rep-tosmap n f as

rep-tosmap′ : ∀ n (f : (x : A) → Set r)
            (p : Product⊤ n (sreplicate n A)) → Sets n (lreplicate n r)
rep-tosmap′ zero f p = _
rep-tosmap′ (n +1) f (a , as) = f a , rep-tosmap′ n f as
