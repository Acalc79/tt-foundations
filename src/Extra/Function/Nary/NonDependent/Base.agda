module Extra.Function.Nary.NonDependent.Base where

open import Data.Fin
open import Data.Product.Nary.NonDependent
open import Data.Sum renaming (_⊎_ to Sum)
open import Foundation
open import Function.Nary.NonDependent
open import Numbers.Nat hiding (_⊔_)

private variable
  a b l I : Level
  A : Set a
  m n : ℕ
  ls ls' : Levels n

Levels-toSets : (ls : Levels n) → Sets n (lmap lsuc n ls)
Levels-toSets {0} ls = _
Levels-toSets {n +1} (l , ls) = Set l , Levels-toSets ls

private
  Prod : ∀{n ls} → Sets n ls → Set (⨆ n ls)
Prod {n} = Product⊤ n

Product⊤-toSets : ∀ n {ls} → Prod (Levels-toSets ls) → Sets n ls
Product⊤-toSets 0 _ = _
Product⊤-toSets (n +1) (A , as) = A , Product⊤-toSets n as

gather-Sets : ∀ ls → Levels-toSets ls ⇉ Sets n ls
gather-Sets {n} _ = curry⊤ₙ n (Product⊤-toSets n)

stabulate : ∀ n {f : Fin n → Level} →
  ((x : Fin n) → Set (f x)) → Sets n (ltabulate n f)
stabulate 0 F = _
stabulate (ℕ.suc n) F = F zero , stabulate n λ x → F (suc x)

sreplicate : ∀ n → Set a → Sets n (lreplicate n a)
sreplicate n A = stabulate n (const A)

-- indexed maps

limap : ∀{i}{I : Set i}(f : I → Level){n} →
  Prod (sreplicate n I) → Levels n
limap f {zero} _ = _
limap f {n +1} (i , is) = f i , limap f is

simap : ∀{i}{I : Set i}
  {flvl : I → Level}
  (f : (i : I) → Set (flvl i))
  {n}(is : Prod (sreplicate n I)) → Sets n (limap flvl is)
simap f {n = zero} _ = _
simap f {n = n +1} (i , is) = f i , simap f is

-- maps with membership

_∈Sets_ : ∀{a}{n}{ls} → Set a → Sets n ls → Set (lsuc a)
_∈Sets_ {n = 0} A _ = Lift _ ⊥
_∈Sets_ {a}{n +1}{l , ls} A (B , Bs) =
  Sum {a = lsuc a} (Σ (a ≡ l) λ {refl → A ≡ B})(A ∈Sets Bs)

p-tolmap : ∀ n {ls}{As : Sets n ls}  
  --------------------------------------------------
  (flvl : ∀{a}{A : Set a}(A∈As : A ∈Sets As) → A → Level) →
  Product⊤ n As → Levels n
p-tolmap 0 flvl x = _
p-tolmap (n +1) flvl (a , as) =
  flvl (inj₁ (refl , refl)) a , p-tolmap n (flvl ∘ inj₂) as

p-tosmap : ∀ n {ls}{As : Sets n ls}  
  --------------------------------------------------
  (flvl : ∀{a}{A : Set a}(A∈As : A ∈Sets As) → A → Level)
  (f : ∀{a}{A : Set a}(A∈As : A ∈Sets As)(x : A) → Set (flvl A∈As x))
  (p : Product⊤ n As) → Sets n (p-tolmap n flvl p)
p-tosmap zero flvl f p = _
p-tosmap (n +1) flvl f (a , as) =
  f (inj₁ (refl , refl)) a ,
  p-tosmap n (flvl ∘ inj₂) (λ A∈As → f (inj₂ A∈As)) as

-- Agda 2.6.2 doesn't recognise that the types in recursive call are correct
{-
module _ {A : Set a}{B : Set b} where
  ∈replicate : ∀ n → A ∈Sets (sreplicate n B) →
    Σ {b = lsuc a}(a ≡ b) λ { refl → A ≡ B}
  ∈replicate (n +1) (inj₁ (refl , A≡B)) = refl , A≡B
  ∈replicate (n +1) (inj₂ A∈Bs) = ∈replicate n A∈Bs'
    where
    A∈Bs' : A ∈Sets (sreplicate n B)
    A∈Bs' = A∈Bs
-}
