module Extra.Function.Nary.SemiDependent where

open import Data.Product.Nary.NonDependent
open import Extra.Data.Product.Nary.NonDependent as Prod
open import Extra.Data.Product.Nary.SemiDependent as Prod
open import Extra.Function.Nary.SemiDependent.Base public
open import Foundation
open import Function.Nary.NonDependent
open import Numbers.Nat hiding (_⊔_)
open import Relation.Nary

private
  variable
    a b c r : Level
    A : Set a
    B : Set b
    C : Set c

-- The type of sliftₙ′ is fairly unreadable. Here it is written with ellipsis:

-- liftₙ : ∀ k n. ((b₁ : B₁) → ⋯ → (bₖ : Bₖ) → R (b₁ , ⋯ , bₖ)) →
--                (f₁ : A₁ → ⋯ → Aₙ → B₁) →
--                       ⋮
--                (fₖ : A₁ → ⋯ → Aₙ → Bₖ) →
--                ((a₁ : A₁) → ⋯ → (aₙ : Aₙ) → R (f₁ (a₁ , ⋯ , aₖ) , ⋯ , fₖ (a₁ , ⋯ , aₖ)))

sliftₙ′ :
  ∀ k n {ls rs}
  {As : Sets n ls}{Bs : Sets k rs}{R : Product⊤ k Bs → Set r} →
  Bs ⇶′ R →
  smap _ (As ⇉_) k Bs ⇶′
    λ fs → As ⇶′
      λ as → R (uncurry⊤ₙ k (Prod.mapₙ n k) fs as)
sliftₙ′ k n op = scurry⊤ₙ′ k λ fs →
                 scurry⊤ₙ′ n λ as →
                 unscurry⊤ₙ′ k op $
                 uncurry⊤ₙ k (Prod.mapₙ n k) fs as
