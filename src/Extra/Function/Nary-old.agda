module Extra.Function.Nary where

open import Algebra
open import Data.Nat.Induction
  using (Acc; acc; <-wellFounded-fast)
open import Extra.Function.Nary.SemiDependent
open import Foundation hiding (_≗_; lift)
open import Function.Nary.NonDependent
open import Numbers.Nat hiding (_⊔_)

private variable
  a b l : Level
  A : Set a
  m n : ℕ
  ls : Levels n

Generalized-≗ : ∀ n {ls}{as : Sets n ls}{B : Set b}(_≈_ : Rel B l)
  (f g : as ⇉ B) → Set (l ⊔ (⨆ n ls))
Generalized-≗ 0 _≈_ f g = f ≈ g
Generalized-≗ (n +1) {as = A , as} _≈_ f g =
  ∀ a → Generalized-≗ n _≈_ (f a)(g a)

lmap-id : lmap id n ls ≡ ls
lmap-id {0} = refl
lmap-id {n +1} {l , ls} = cong (l ,_) lmap-id

slift : ∀ n {ls}(pre : Sets n ls){m ls'}
  (as : Sets m ls') → Sets m (lmap (_⊔ ⨆ n ls) m ls')
slift n {ls} pre {m} = smap (_⊔ ⨆ n ls) (pre ⇉_) m 

lsubst : {x y : A}{lvl : A → Level}
  (f : (x : A) → Set (lvl x))(eq : x ≡ y) → f x → f y
lsubst f refl = id

slift-0 : ∀{pre : Sets 0 _}{m ls'}(as : Sets m ls')
  → ------------------------------------------------------------
  slift 0 pre as ≡ lsubst (Sets m)(sym lmap-id) as
slift-0 = {!!}

lift-1 : (P : Set l) → n {ls}(as : Sets n ls){B : Set b}
  (f : as ⇉ B) → slift 1 (P , _) as ⇉ P → B
lift-1 = ?

lift′ : ∀ m {ls}(pre : Sets m ls)
  n {ls'}(as : Sets n ls'){B : Set b}
  (f : as ⇉ B) → Acc _<_ (m + n) → slift m pre as ⇉ (pre ⇉ B)
lift′ 0 pre 0 as f rec = f
lift′ 0 pre (n +1) (A , as) f (acc rs) a =
  lift′ 0 pre n as (f a) (rs n $ n<1+n n)
lift′ (m +1) (P , ps) 0 as f (acc rs) p =
  lift′ m ps 0 as f (rs (m + 0) $ n<1+n (m + 0))
lift′ (m +1) (P , ps) (n +1) (A , as) {B} f (acc rs) =
  {!lift′ m ps (n +1) (A , as){B} f _!}
-- (P → ps ⇉ A) → smap _ (λ Aᵢ → P → ps ⇉ Aᵢ) n as ⇉ (P → Arrows m ps B)

lift : ∀ m {ls}(pre : Sets m ls)
  n {ls'}(as : Sets n ls'){b}{B : Set b}
  (f : as ⇉ B) → slift m pre as ⇉ (pre ⇉ B)
lift m pre n as f = {!!}

Property : ∀ n {ls}(as : Sets n ls) → as ⇉ Set b → Set (b ⊔ ⨆ n ls)
Property 0 as P = P
Property (n +1)(A , as) P = ∀ a → Property n as (P a)

module Test where

  comm-is-property : {A : Set a}(_∙_ : Op₂ A) →
    Commutative _≡_ _∙_ ≡ Property 2 (A , A , _) λ a b → a ∙ b ≡ b ∙ a
  comm-is-property _∙_ = refl

  assoc-is-property : {A : Set a}(_∙_ : Op₂ A) →
    Associative _≡_ _∙_ ≡
    Property 3 (A , A , A , _) λ a b c → (a ∙ b) ∙ c ≡ a ∙ (b ∙ c)
  assoc-is-property _∙_ = refl

  left-identity-is-property : {A : Set a}(_∙_ : Op₂ A)(e : A) →
    LeftIdentity _≡_ e _∙_ ≡
    Property 1 (A , _) λ a → e ∙ a ≡ a
  left-identity-is-property e _∙_ = refl
