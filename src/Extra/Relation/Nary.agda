module Extra.Relation.Nary where

open import Algebra
open import Data.Product.Nary.NonDependent
open import Extra.Data.Product.Nary.NonDependent as Prod
open import Extra.Data.Product.Nary.SemiDependent
open import Extra.Function.Nary.NonDependent
open import Extra.Function.Nary.SemiDependent
open import Foundation hiding (Universal)
open import Function.Nary.NonDependent
open import Numbers.Nat hiding (_⊔_)
open import Relation.Nary

private variable
  a b l r : Level
  A : Set a
  m n : ℕ
  ls ls' : Levels n

Property-of′ :
  ∀ m {ls}(As : Sets m ls){n ls'}{Args : Sets n ls'} →
  As ⇉ Args ⇉ Set b → As ⇉ Set (b ⊔ ⨆ n ls')
Property-of′ m _ = liftₙ 1 m Π[_]

Property-of :
  ∀ m {ls : Levels m}{n ls'}{Args : Sets n ls'}{b} →
  Levels-toSets ls ⇶′ λ p →
  let As = Product⊤-toSets m p in As ⇉ Args ⇉ Set b → As ⇉ Set (b ⊔ ⨆ n ls')
Property-of m = scurry⊤ₙ′ m λ as → Property-of′ m (Product⊤-toSets m as)

private
  Prod : ∀{m ls} → Sets m ls → Set _
  Ops = λ {b}(B : Set b){m} → rep-tosmap m (λ o → Opₙ o B)
Prod = Product⊤ _

AlgebraicEqProperty′ : ∀ n m →
  (ns : Prod (sreplicate m ℕ)) →
  (∀{a r}{A : Set a} → Rel A r → Prod (Ops A ns) → Prod (sreplicate n A) → Set r) →
  ∀(A : Set a) r → Rel A r → Prod (Ops A ns) → Set (r ⊔ ⨆ n (lreplicate n a))
AlgebraicEqProperty′ n m ns prop A r _≈_ ops = Π[ curry⊤ₙ n (prop _≈_ ops)  ]

-- syntax:
-- AlgebraicEqProperty
--   [# variables]
--   [# operators]
--   [[arities of each operator]]
--   [lambda defining the property]
--   [carrier set]
--   [equality relation level]
-- see module Test for examples
AlgebraicEqProperty : ∀ n m →
  sreplicate m ℕ ⇶ω λ ns →
  (∀{a r}{A : Set a} → Rel A r → Ops A ns ⇉ sreplicate n A ⇉ Set r) →
  ∀(A : Set a) r → Rel A r → Ops A ns ⇉ Set (r ⊔ ⨆ n (lreplicate n a))
AlgebraicEqProperty n m =
  scurry⊤ₙω m λ ns prop A r _≈_ →
  curry⊤ₙ m λ ops →
  AlgebraicEqProperty′ n m ns
    (λ _≈_ → uncurry⊤ₙ n ∘ uncurry⊤ₙ m (prop _≈_)) A r _≈_ ops

PointwiseBinRelₙ : ∀ n {ls}{As : Sets n ls}{B : Set b}(_≈_ : Rel B l)
  (f g : As ⇉ B) → Set (l ⊔ (⨆ n ls))
PointwiseBinRelₙ n _≈_ f g = Π[ liftₙ 2 n _≈_ f g ]

Pointwise≡Function≡ : ∀ n {ls}{As : Sets n ls}{B : Set b}
  (f g : As ⇉ B) → Set (b ⊔ (⨆ n ls))
Pointwise≡Function≡ n = PointwiseBinRelₙ n _≡_

syntax Pointwise≡Function≡ n f g = f ≗[ n ] g

-- special version of liftₙ for Opₙ
-- cannot implement using liftₙ as Opₙ is not defined in terms of _⇉_
PointwiseOpₙ : ∀{n}{ls}(As : Sets n ls) m {B : Set b} →
  Opₙ m B → Opₙ m (As ⇉ B)
PointwiseOpₙ {n = n} As m op =
  curryOp⊤ m λ fs →
  curry⊤ₙ n λ as →
  uncurryOp⊤ m op $
  zipapply m n fs as
  where
  zipapply : ∀ m n {ls}{As : Sets n ls}{B : Set b}
    (fs : Prod (sreplicate m (As ⇉ B)))(as : Prod As) →
    Prod (sreplicate m B)
  zipapply zero n _ _ = _
  zipapply (m +1) n (f , fs) as = uncurry⊤ₙ n f as , zipapply m n fs as

pointwiseOpₙ-n≡0 : ∀{A : Set a}{As : Sets 0 _} n
  (op : Opₙ n A) → PointwiseOpₙ As n op ≗[ n ] op
pointwiseOpₙ-n≡0 zero op = erefl op
pointwiseOpₙ-n≡0 (n +1) op x = pointwiseOpₙ-n≡0 n (op x)

pointwiseLiftOps :
  ∀{n}{ls}(As : Sets n ls)
  {m}{ns : Prod (sreplicate m ℕ)}{B : Set b} →
  Prod (Ops B ns) → Prod (Ops (As ⇉ B) ns)
pointwiseLiftOps _  {zero} _ = _
pointwiseLiftOps As {m +1}{n , _} (op , ops) =
  PointwiseOpₙ As n op , pointwiseLiftOps As ops 

pointwiseLiftOps-n≡0 : 
  ∀{As : Sets 0 _}
  {m}{ns : Prod (sreplicate m ℕ)}{B : Set b}
  (ops : Prod (Ops B ns)) →
  pointwiseLiftOps As ops ≡ ops
pointwiseLiftOps-n≡0 {m = zero} ops = erefl ops
pointwiseLiftOps-n≡0 {m = m +1}{n , _} (op , ops) =
  cong₂ _,_ {!!} (pointwiseLiftOps-n≡0 ops)

inherit′ :
  ∀{n}{ls}{As : Sets n ls} -- lift through these
  {A : Set a}{m}{ns : Prod (sreplicate m ℕ)}{o}{r} → -- property of these
  (prop : ∀{a r}{A : Set a} →
          Rel A r → Prod (Ops A ns) →
          Prod (sreplicate o A) → Set r) -- defined by this
  (_≈_ : Rel A r)(ops : Prod (Ops A ns))
  (holds : AlgebraicEqProperty′ o m ns prop A r _≈_ ops) →
  AlgebraicEqProperty′ o m ns prop (As ⇉ A)(r ⊔ ⨆ n ls)
    (PointwiseBinRelₙ n _≈_)(pointwiseLiftOps As ops)
inherit′ {n = 0}   {_}           prop _≈_ ops holds = {!holds!}
inherit′ {n = n +1}{As = A , As} prop _≈_ ops holds = {!!}
-- inherit′ {n = zero}{_}{As}{A}{m}{ns}{o}{r} prop _≈_ ops holds = {!holds!}
-- inherit′ {n = _+1 n} prop _≈_ ops holds = {!!}

-- inherit :
--   ∀{n}{ls}{As : Sets n ls} -- lift through these
--   {A : Set a}{m}{ns : Product⊤ m (sreplicate m ℕ)}{o}{r}{b} → -- property of these
--   let Ops = rep-tosmap′ m (λ o → Opₙ o A) ns
--       Args = sreplicate o A
--   in (prop : Rel A r → Ops ⇉ Args ⇉ Set b) -- defined by this
--   (_≈_ : Rel A r) →
--   Ops ⇶′ λ ops →
--   (holds : uncurry⊤ₙ m (unscurry⊤ₙ′ m (AlgebraicEqProperty A m r {b}) ns prop _≈_) ops) →
--   uncurry⊤ₙ m (
--     unscurry⊤ₙ′ m (
--       AlgebraicEqProperty (As ⇉ A) m (r ⊔ ⨆ n ls) {b})
--       ns (λ _lft≈_ → curry⊤ₙ m λ lft-ops → curry⊤ₙ o λ lft-args → {!!})
--          (λ f g → Π[ liftₙ 2 n {as = As} _≈_ f g ]))
--       {!!}
-- inherit = {!!}  

module Test where
  comm-is-property :
    Commutative {ℓ = b}{A = A} ≡
    Property-of 2 _ _ λ _≈_ _∙_ a b → (a ∙ b) ≈ (b ∙ a)
  comm-is-property = refl

  assoc-is-property :
    Associative ≡
    Property-of 2 (Rel A a)(Op₂ A) λ _≈_ _∙_ a b c → ((a ∙ b) ∙ c) ≈ (a ∙ (b ∙ c))
  assoc-is-property = refl

  left-identity-is-property :
    LeftIdentity ≡
    Property-of 3 (Rel A b) A (Op₂ A) λ _≈_ e _∙_ a → (e ∙ a) ≈ a
  left-identity-is-property = refl

  comm-is-algebraiceq :
    Commutative ≡
    AlgebraicEqProperty 2 1 2 (λ _≈_ _∙_ a b → (a ∙ b) ≈ (b ∙ a)) A r
  comm-is-algebraiceq = refl

  assoc-is-algebraiceq :
    Associative ≡
    AlgebraicEqProperty 3 1 2 (λ _≈_ _∙_ a b c → ((a ∙ b) ∙ c) ≈ (a ∙ (b ∙ c))) A r
  assoc-is-algebraiceq = refl

  left-identity-is-algebraiceq :
    LeftIdentity ≡
    AlgebraicEqProperty 1 2 0 2 (λ _≈_ e _∙_ a → (e ∙ a) ≈ a) A r
  left-identity-is-algebraiceq = refl
