module Courses.NumbersAndSets.Division.Primes where

import Data.Fin as Fin
import Data.Fin.Properties as Fin
open import Data.List hiding (_─_)
open import Data.List.Membership.Propositional as Mem using (_∈_)
open import Data.List.Relation.Binary.Permutation.Propositional
  using (_↭_; ↭-reflexive; module PermutationReasoning)
  renaming (module _↭_ to ↭)
import Data.List.Properties as List
open import Data.List.Relation.Unary.All as All using (All; _∷_; [])
open import Data.List.Relation.Unary.All.Properties as All using (++⁺)
open import Data.List.Relation.Unary.Any as Any
  using (Any; here; there; _─_)
import Data.Nat.Coprimality as Coprime
open import Data.Nat.Divisibility
open import Data.Nat.DivMod
open import Data.Nat.GCD
open import Data.Nat.Induction
  using (Acc; acc; <-wellFounded-fast)
open import Data.Nat.Primality
open import Data.Nat.Solver
open import Extra.Data.List
open import Foundation hiding (_∈_)
open import Numbers.Nat
open import Relation.Nullary.Product using (_×-dec_)

open +-*-Solver

private variable
  m n o p : ℕ

prime-divisor : ∀{m p} → Prime p → m ∣ p → m ≡ 1 ⊎ m ≡ p
prime-divisor {0}{_ +1} _ m∣p = ⊥-elim $ 1+n≢0 $ 0∣⇒≡0 m∣p
prime-divisor {1} _ _ = inj₁ refl
prime-divisor {m +2} {p +2} prime-p+2 m+2∣p+2 with p ≤? m
... | yes p≤m = inj₂ $ ≤-antisym (∣⇒≤ m+2∣p+2) (s≤s $ s≤s p≤m)
prime-divisor {m +2} {p +2} prime-p+2 m+2∣p+2 | no p≰m =
  contradiction m+2∣p+2 $
  subst (_∤ p +2) (cong _+2 $ Fin.toℕ-fromℕ< (≰⇒> p≰m)) $
  prime-p+2 (Fin.fromℕ< (≰⇒> p≰m))

prime-partition : ∀ n
    → --------------------------------------------------
    n ≤ 1 ⊎ Prime n ⊎ Σ[ m ∈ ℕ ] m +2 < n × m +2 ∣ n

prime-partition 0 = inj₁ z≤n
prime-partition 1 = inj₁ (s≤s z≤n)
prime-partition (n +2) = inj₂ $
  case Fin.any? {n} (λ i → Fin.toℕ i +2 ∣? n +2) of λ
  { (no ¬∃div) → inj₁ λ i i+2∣n+2 → flip contradiction ¬∃div $ i , i+2∣n+2
  ; (yes (m , m+2∣n+2)) → inj₂ (Fin.toℕ m , s≤s (s≤s $ Fin.toℕ<n m) , m+2∣n+2)}

prime>1 : Prime p → p > 1
prime>1 {n +2} _ = s≤s (s≤s z≤n)

prime≢0 : Prime p → p ≢ 0
prime≢0 () refl

prime≢1 : Prime p → p ≢ 1
prime≢1 () refl

¬Prime⇒divisor : {True (n >? 1)} → ¬ Prime n → Σ[ m ∈ ℕ ] m +2 < n × m +2 ∣ n
¬Prime⇒divisor {n}{n>1} ¬prime-n = case prime-partition n of λ
  { (inj₁ n≤1) → contradiction (toWitness n>1) (≤⇒≯ n≤1)
  ; (inj₂ (inj₁ prime-n)) → contradiction prime-n ¬prime-n 
  ; (inj₂ (inj₂ ∃divisor)) → ∃divisor}

product-of-primes′ : ∀ n → {True (1 <? n)} → Acc _<_ n
  → --------------------------------------------------
  Σ[ ps ∈ List ℕ ] length ps > 0 × All Prime ps × product ps ≡ n
product-of-primes′ n {n>1} (acc rec) = case prime-partition n of λ
  { (inj₁ n≤1) → contradiction n≤1 (<⇒≱ $ toWitness n>1)
  ; (inj₂ (inj₁ prime-n)) → [ n ] , s≤s z≤n , prime-n ∷ [] , *-identityʳ n
  ; (inj₂ (inj₂ (m , m+2<n , m+2∣n))) → let
    o = n / (m +2)
    o<n : o < n
    o<n = m/n<m n (m +2)(<⇒≤ $ toWitness n>1)(s≤s $ s≤s z≤n)
    o∣n : o ∣ n
    o∣n = m/n∣m m+2∣n
    1<o : 1 < o
    1<o = 1<m/n m+2<n m+2∣n
    prod-m+2 = product-of-primes′ (m +2)(rec (m +2) m+2<n)
    prod-o = product-of-primes′ o {fromWitness 1<o} (rec o o<n)
    in case prod-m+2 , prod-o of λ
    { ((ps₁ , len-ps₁>0 , prime-ps₁ , prod-ps₁) ,
       (ps₂ , len-ps₂>0 , prime-ps₂ , prod-ps₂)) →
      ps₁ ++ ps₂ ,
      subst (_> 0) (sym $ List.length-++ ps₁)
        (+-mono-< len-ps₁>0 len-ps₂>0) ,
      ++⁺ prime-ps₁ prime-ps₂ , (begin
      product (ps₁ ++ ps₂) ≡⟨ List.foldr-++ _*_ 1 ps₁ ps₂ ⟩
      foldr _*_ (product ps₂) ps₁
        ≡⟨ cong (λ o → foldr _*_ o ps₁) prod-ps₂ ⟩
      foldr _*_ o ps₁ ≡˘⟨ cong (λ o → foldr _*_ o ps₁) $ *-identityʳ o ⟩
      foldr _*_ (o * 1) ps₁
        ≡˘⟨ List.foldr-fusion (o *_) 1
             (λ m n → solve 3 (λ m n o → o :* (m :* n) := m :* (o :* n))
                            (λ{_}{_}{_} → refl) m n o) ps₁ ⟩
      o * foldr _*_ 1 ps₁ ≡⟨ cong (o *_) prod-ps₁ ⟩
      o * m +2 ≡⟨ m/n*n≡m m+2∣n ⟩
      n ∎)}}
  where
  open ≡-Reasoning

product-of-primes :
  ∀ n → {True (n >? 1)} → Σ[ ps ∈ List ℕ ] length ps > 0 × All Prime ps × product ps ≡ n
product-of-primes (n +2) =
  product-of-primes′ (n +2) (<-wellFounded-fast (n +2)) 

all-primes< :
  ∀ n →
  Σ[ ps ∈ List ℕ ] All Prime ps × ∀ m → m < n → Prime m → m ∈ ps
all-primes< 0 = [] , [] , λ _ ()
all-primes< (n +1) = case all-primes< n of λ
  { (ps , ps-prime , all∈ps) → case prime? n of λ
  { (yes prime-n) → n ∷ ps , prime-n ∷ ps-prime ,
    λ m m<n+1 prime-m → case m <? n of λ
    { (no m≮n) → here (≤∧≮⇒≡ (≤-pred m<n+1) m≮n)
    ; (yes m<n) → there (all∈ps m m<n prime-m)}
  ; (no ¬prime-n) → ps , ps-prime , λ m m<n+1 prime-m → case m <? n of λ
    { (no m≮n) →
      contradiction (subst Prime (≤∧≮⇒≡ (≤-pred m<n+1) m≮n) prime-m)
                    ¬prime-n
    ; (yes m<n) → all∈ps m m<n prime-m}}}

m∣any⇒m∣prod : ∀{ms} → Any (m ∣_) ms → m ∣ product ms
m∣any⇒m∣prod {m}{ms} x =
  List.foldr-preservesᵒ (λ
    { n o (inj₁ m∣n) → ∣m⇒∣m*n o m∣n
    ; n o (inj₂ m∣o) → ∣n⇒∣m*n n m∣o}) 1 ms (inj₂ x)

m∣prod : ∀{ms} → m ∈ ms → m ∣ product ms
m∣prod = m∣any⇒m∣prod ∘ Any.map ∣-reflexive

∞-primes : ∀ n → Σ[ p ∈ ℕ ] p > n × Prime p
∞-primes n = case all-primes< (n +1) of λ
  { (ps , prime-ps , all∈ps) → let
    N = product ps +1
    N>1 : N > 1
    N>1 = s≤s $
      List.foldr-preservesᵇ {P = 0 <_}
        (λ {m}{n} 0<m 0<n → ≤-trans 0<m $ m≤m*n m 0<n) ≤-refl $
      All.map (<-trans (s≤s z≤n) ∘ prime>1) prime-ps
    ∃prime≤N? : Dec (∃ λ m → Fin.toℕ m > n × Prime (Fin.toℕ m))
    ∃prime≤N? =
      Fin.any? {N +1} λ m → Fin.toℕ m >? n ×-dec prime? (Fin.toℕ m)
    in case ∃prime≤N? of λ
    { (yes (m , m>n , prime-m)) → Fin.toℕ m , m>n , prime-m
    ; (no ¬∃prime≤N) → case product-of-primes N {fromWitness N>1} of λ
      { (p@(p'@(_ +1) +1) ∷ ps' , _ , p-prime ∷ ps'-prime , product≡N) → let
        p∣N : p ∣ N
        p∣N = subst (p ∣_) product≡N $ ∣m⇒∣m*n (product ps') ∣-refl
        p>1 : p > 1
        p>1 = prime>1 p-prime
        p<N+1 : p < N +1
        p<N+1 = s≤s $ ∣⇒≤ p∣N
        in case p <? n +1 of λ
        { (yes p<n+1) → let
          p∈ps : p ∈ ps
          p∈ps = all∈ps p p<n+1 p-prime
          in ⊥-elim $ 1+n≢0 $ begin
          1 ≡⟨⟩
          1 % p ≡˘⟨ %-remove-+ʳ 1 $ m∣prod p∈ps ⟩
          N % p ≡⟨ n∣m⇒m%n≡0 N p' p∣N ⟩
          0 ∎
        ; (no p≮n+1) → flip contradiction ¬∃prime≤N $
        Fin.fromℕ< {p} p<N+1 ,
        subst (n <_) (sym $ Fin.toℕ-fromℕ< p<N+1) (≮⇒≥ p≮n+1) ,
        subst Prime (sym $ Fin.toℕ-fromℕ< p<N+1) p-prime}}}}
  where open ≡-Reasoning

m∣no∧gcd≡1⇒m∣o : m ∣ n * o → GCD m n 1 → m ∣ o
m∣no∧gcd≡1⇒m∣o m∣no gcd≡1 =
  Coprime.coprime-divisor (Coprime.GCD≡1⇒coprime gcd≡1) m∣no

p∣m*n⇒p∣m∨p∣n : Prime p → p ∣ m * n → p ∣ m ⊎ p ∣ n
p∣m*n⇒p∣m∨p∣n {p}{m}{n} p-prime p∣mn =
  case prime-divisor p-prime (gcd[m,n]∣m p m) of λ
  { (inj₁ gcd≡1) →
    inj₂ $ m∣no∧gcd≡1⇒m∣o p∣mn $ subst (GCD p m) gcd≡1 $ gcd-GCD p m
  ; (inj₂ gcd≡p) →
    inj₁ $ ∣-trans (subst (_∣ gcd p m) gcd≡p ∣-refl) $ gcd[m,n]∣n p m}

p∣prod⇒p∣any : Prime p → ∀{ms} → p ∣ product ms → Any (p ∣_) ms
p∣prod⇒p∣any p-prime {ms} p∣prod =
  Sum.fromInj₁ (λ p∣1 → ⊥-elim $ prime≢1 p-prime $ ∣1⇒≡1 p∣1) $
  foldr-forcesᵒ (λ _ _ → p∣m*n⇒p∣m∨p∣n p-prime) 1 ms p∣prod

private
  prod‿─ : ∀{n ns}{≢0 : False (n ≟ 0)}(n∈ns : n ∈ ns)
    → --------------------------------------------------
    product (ns ─ n∈ns) ≡ (product ns / n){≢0}
  prod‿─ {n = n@(_ +1)}{n ∷ ns}(here refl) =
    sym $ m*n/m≡n n (product ns)
  prod‿─ {n = n@(_ +1)}{n' ∷ ns}(there n∈ns) = begin
    n' * product (ns ─ n∈ns) ≡⟨ cong (n' *_) $ prod‿─ n∈ns ⟩
    n' * (product ns / n) ≡˘⟨ *-/-assoc n' $ m∣prod n∈ns ⟩
    n' * product ns / n ∎
    where open ≡-Reasoning

factors-unique : {n≥1 : True (n ≥? 1)} → ∀{ps ps'} →
  All Prime ps × product ps ≡ n →
  All Prime ps' × product ps' ≡ n
  → --------------------------------------------------
  ps ↭ ps'
factors-unique {1}{ps = ps}{qs} Pps Pqs =
  ↭-reflexive $ trans (lemma ps Pps)(sym $ lemma qs Pqs)
  where
  lemma : ∀ ls → All Prime ls × product ls ≡ 1 → ls ≡ []
  lemma [] _ = refl
  lemma (p ∷ ps)(p-prime ∷ ps-prime , p*prod≡1) = ⊥-elim $
    prime≢1 p-prime $ m*n≡1⇒m≡1 p (product ps) p*prod≡1
factors-unique {n +2}{ps = p ∷ ps}{qs}
  (p-prime ∷ ps-prime , prod-ps≡n)(qs-prime , prod-qs≡n) =
  let p∣qs : p ∣ product qs
      p∣qs = subst (p ∣_) (trans prod-ps≡n (sym prod-qs≡n)) $
             m∣m*n (product ps)
      ∃q:p∣q : ∃ λ q → q ∈ qs × p ∣ q
      ∃q:p∣q = Mem.find (p∣prod⇒p∣any p-prime p∣qs)
      q = proj₁ ∃q:p∣q
      q∈qs = proj₁ $ proj₂ ∃q:p∣q
      p∣q = proj₂ $ proj₂ ∃q:p∣q
      q-prime : Prime q
      q-prime = All.lookup qs-prime q∈qs
      p≡q : p ≡ q
      p≡q = Sum.fromInj₂ (⊥-elim ∘ prime≢1 p-prime) $
            prime-divisor q-prime p∣q
      prod-ps≥1 : product ps ≥ 1
      prod-ps≥1 =
        List.foldr-preservesᵇ
          (λ { {x +1} {y +1} x≥1 y≥1 → s≤s z≤n})
          ≤-refl $
        All.map (<⇒≤ ∘ prime>1) ps-prime
      prod-qs─q≡prod-ps : product (qs ─ q∈qs) ≡ product ps
      prod-qs─q≡prod-ps = let
        p≢0 = fromWitnessFalse (prime≢0 p-prime)
        q≢0 = fromWitnessFalse (prime≢0 q-prime)
        open ≡-Reasoning
        in begin
        product (qs ─ q∈qs) ≡⟨ prod‿─ {≢0 = q≢0} q∈qs ⟩
        product qs / q      ≡˘⟨ /-congʳ {n≢0 = p≢0} p≡q ⟩
        product qs / p
          ≡⟨ cong (_/ p) $ trans prod-qs≡n (sym prod-ps≡n) ⟩
        p * product ps / p  ≡⟨ m*n/m≡n p (product ps) ⟩
        product ps ∎
  in let open PermutationReasoning in begin
  p ∷ ps ≡⟨ cong (_∷ ps) p≡q ⟩
  q ∷ ps ↭⟨ ↭.prep q $
    factors-unique {n≥1 = fromWitness prod-ps≥1} (ps-prime , refl) $
    All.─⁺ q∈qs qs-prime , prod-qs─q≡prod-ps ⟩
  q ∷ (qs ─ q∈qs) ↭˘⟨ ∈↭hd q∈qs ⟩
  qs ∎

fundamental-theorem-of-arithmetic : True (n >? 0) →
  ∃! _↭_ λ ps → All Prime ps × product ps ≡ n
fundamental-theorem-of-arithmetic {1} _ =
  [] , ([] , refl) ,
  λ { {[]} _ → ↭.refl
    ; {p ∷ ps} (p-prime ∷ ps-prime , p*prod-ps≡1) →
      contradiction (prime>1 p-prime) $
      ≤⇒≯ $ ≤-reflexive $ m*n≡1⇒m≡1 p (product ps) p*prod-ps≡1 }
fundamental-theorem-of-arithmetic {n +2} _ with product-of-primes (n +2){_}
... | ps , _ , ps-prime , prod≡n+2 =
  ps , (ps-prime , prod≡n+2) , factors-unique (ps-prime , prod≡n+2)
