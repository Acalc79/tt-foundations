module Courses.NumbersAndSets.CountingAndIntegers.BasicCounting where

open import Data.Bool
open import Data.Bool
  renaming
  ( Bool to Two; true to ₁; false to ₀
  ; not to 1-_; _∨_ to _+_; _∧_ to _*_; _xor_ to _-_)
open import Foundation as F hiding (_∪_; _∩_)
open import Function.Equality using (≡-setoid)
open import Relation.Unary.Properties

private variable
  a l : Level
  A X : Set a
  x : X

Subset : (A : Set a) → Set _
Subset A = Σ[ P ∈ ℙ A ] Un.Decidable P

_∩_ _∪_ _╲_ : (A B : Subset X) → Subset X
bar : (A : Subset X) → Subset X
(A , A?) ∩ (B , B?) = A F.∩ B , A? ∩? B?
(A , A?) ∪ (B , B?) = A F.∪ B , A? ∪? B?
bar (A , A?) = ∁ A , ∁? A?
A ╲ B = A ∩ bar B

i : (A : Subset X) → X → Two
i (_ , P?) a = does (P? a)

module _ {X : Set a} where
  private
    module _ ⦃ A : Subset X ⦄ where
      private
        A? = proj₂ A
        PredA = proj₁ A
  
      x∈A⇒iAx≡1 : x ∈ PredA → i A x ≡ ₁
      x∈A⇒iAx≡1 {x} x∈A with A? x
      ... | yes x∈A = refl
      ... | no x∉A = contradiction x∈A x∉A
  
      iAx≡1⇒x∈A : i A x ≡ ₁ → x ∈ PredA
      iAx≡1⇒x∈A {x} iAx with A? x
      ... | yes x∈A = x∈A
      iAx≡1⇒x∈A () | no _
  
      x∉A⇒iAx≡0 : x ∉ PredA → i A x ≡ ₀
      x∉A⇒iAx≡0 {x} x∉A with A? x
      ... | yes x∈A = contradiction x∈A x∉A
      ... | no x∉A = refl
  
      iAx≡0⇒x∉A : i A x ≡ ₀ → x ∉ PredA
      iAx≡0⇒x∉A {x} iAx with A? x
      ... | no x∉A = x∉A
      iAx≡0⇒x∉A () | yes _
  
  module _ {A B : Subset X} where
    private
      _∈A? = proj₂ A
      _∈B? = proj₂ B
      PredA = proj₁ A
      PredB = proj₁ B
      instance
        _ = A
        _ = B
  
    prop-0 : i A ≗ i B ⇔ PredA ≅ PredB
    prop-0 = mk⇔ iA≡iB⇒A≡B A≡B⇒iA≡iB
      where
      iA≡iB⇒A≡B : i A ≗ i B → PredA ≅ PredB
      iA≡iB⇒A≡B iA≡iB x = mk⇔
        (iAx≡1⇒x∈A ∘ subst (_≡ ₁)(iA≡iB x) ∘ x∈A⇒iAx≡1)
        (iAx≡1⇒x∈A ∘ subst (_≡ ₁)(sym $ iA≡iB x) ∘ x∈A⇒iAx≡1)
      A≡B⇒iA≡iB : PredA ≅ PredB → i A ≗ i B
      open F.Equivalence
      A≡B⇒iA≡iB A≅B x with x ∈A? | x ∈B?
      ... | no _ | no _ = erefl ₀
      ... | no x∉A | yes x∈B = contradiction (g (A≅B x) x∈B) x∉A
      ... | yes x∈A | no x∉B = contradiction (f (A≅B x) x∈A) x∉B
      ... | yes _ | yes _ = erefl ₁

    infixl 8 _⊛_
    infixl 7 _⊕_ _⊝_
    infixr 7 1⊝_
    _⊕_ _⊝_ _⊛_ : (i₁ i₂ : X → Two) → X → Two
    1⊝_ : (i : X → Two) → X → Two
    (i₁ ⊕ i₂) x = i₁ x + i₂ x
    (i₁ ⊛ i₂) x = i₁ x * i₂ x
    (i₁ ⊝ i₂) x = i₁ x - i₂ x
    (1⊝ i) x = 1- i x

    prop-1 : i (A ∩ B) ≗ i A ⊛ i B
    prop-1 x with does (x ∈A?) | does (x ∈B?)
    ... | false | _ = erefl ₀
    ... | true | false = erefl ₀
    ... | true | true = erefl ₁

    prop-2 : i (bar A) ≗ 1⊝ i A
    prop-2 x with does (x ∈A?)
    ... | true = erefl ₀
    ... | false = erefl ₁

    prop-3 : i (bar (A ∪ B)) ≗ i (bar A ∩ bar B)
    prop-3 x with does (x ∈A?) | does (x ∈B?)
    ... | false | false = erefl ₁
    ... | false | true = erefl ₀
    ... | true | false = erefl ₀
    ... | true | true = erefl ₀

    prop-4 : i (A ∪ B) ≗ i A ⊕ i B ⊝ i (A ∩ B)
    prop-4 = {!!}

