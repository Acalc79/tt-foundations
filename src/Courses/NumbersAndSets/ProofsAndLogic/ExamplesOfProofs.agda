module Courses.NumbersAndSets.ProofsAndLogic.ExamplesOfProofs where

open import Data.Fin.Patterns
open import Data.Integer.Coprimality
open import Data.Integer.Divisibility.Signed
import Data.Nat.Divisibility as ℕ
open import Foundation
open import Numbers.Nat as ℕ using (_+1; _+2; _+3)
open import Numbers.Integer renaming (∣_∣ to abs)
open import Numbers.Integer.Literals
open ℤ-Solver

-- lemma useful for proposition 0
lemma-div3 : ∀ i → 3 ∣ i ⊎ 3 ∣ (1 + i) ⊎ 3 ∣ (2 + i)
lemma-div3 (+ 0) = inj₁ (3 ∣0)
lemma-div3 (+ (n +1)) = case lemma-div3 (+ n) of λ
  { (inj₁ 3∣n) → inj₂ $ inj₂ $ ∣m∣n⇒∣m+n ∣-refl 3∣n
  ; (inj₂ (inj₁ 3∣n+1)) → inj₁ 3∣n+1
  ; (inj₂ (inj₂ 3∣n+2)) → inj₂ (inj₁ 3∣n+2) }
lemma-div3 -[1+ 0 ] = inj₂ (inj₁ (3 ∣0))
lemma-div3 -[1+ 1 ] = inj₂ $ inj₂ (3 ∣0)
lemma-div3 -[1+ 2 ] = inj₁ (∣m⇒∣-m ∣-refl)
lemma-div3 -[1+ n +3 ] with lemma-div3 -[1+ n +2 ]
... | inj₁ 3∣-n-3 = inj₂ (inj₁ 3∣-n-3)
... | inj₂ (inj₁ 3∣-n-2) = inj₂ (inj₂ 3∣-n-2)
... | inj₂ (inj₂ 3∣-n-1) = inj₁ $
  ∣m∣n⇒∣m+n (∣m⇒∣-m ∣-refl) 3∣-n-1 

proposition-0 : ∀ n → 3 ∣ (+ n) ^ 3 - + n
proposition-0 n' = subst (3 ∣_) [n-1]n[n+1]≡n³-n $
  case lemma-div3 (n - 1) of λ
  { (inj₁ 3∣n-1) → ∣m⇒∣m*n (n + 1) $ ∣m⇒∣m*n n 3∣n-1
  ; (inj₂ (inj₁ 3∣n)) → ∣m⇒∣m*n (n + 1) $ ∣n⇒∣m*n (n - 1) $
    subst (3 ∣_)
      (solve 1 (λ n → con 1 :+ (n :- con 1) := n)(λ{_} → refl) n) 3∣n
  ; (inj₂ (inj₂ 3∣n+1)) →
    ∣n⇒∣m*n ((n - 1) * n) $
    subst (3 ∣_)
      (solve 1 (λ n → con 2 :+ (n :- con 1) := n :+ con 1)(λ{_} → refl) n)
      3∣n+1}
  where
  n = + n'
  [n-1]n[n+1]≡n³-n : (n - 1) * n * (n + 1) ≡ n ^ 3 - n
  [n-1]n[n+1]≡n³-n =
    solve 1 (λ n → (n :- con 1) :* n :* (n :+ con 1) := n :^ 3 :- n)
          (λ {_} → refl) n

proposition-1 : ∀ i → Even (i ²) → Even i
proposition-1 i even-i² with i modFin 2 | ≡ₘmodFin {2} i
... | 0F | i≡₂0 = fromWitness i≡₂0
... | 1F | i≡₂1 = ⊥-elim $
  even⇒¬odd (i ²) even-i² $ fromWitness $ *-cong-≡ₘ i≡₂1 i≡₂1

proposition-2 : let f = λ x → x ² - 5 * x + 6
  in --------------------------------------------------
  f 2 ≡ 0 × f 3 ≡ 0 × ∀ x → f x ≡ 0 → x ≡ 2 ⊎ x ≡ 3
proposition-2 = refl , refl , λ x fx≡0 →
  m*n≡0⇒m≡0∨n≡0 (x - 2) (
    trans (solve 1 (λ x → (x :- con 2) :* (x :- con 3) :=
                          x :* x :- con 5 :* x :+ con 6)
                 (λ{_} → refl) x) fx≡0) ∶ x - 2 ≡ 0 ⊎ x - 3 ≡ 0
  |> Sum.map (m-n≡0⇒m≡n x 2)(m-n≡0⇒m≡n x 3)
