module Relation.Nullary.Extra where

by-contradiction : (dec : Decidable P) → ¬ ¬ P → P
