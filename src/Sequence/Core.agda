module Sequence.Core where

open import Foundation

private variable
  a : Level
  A : Set a

Sequence : Set a → Set a
Sequence A = ℕ → A
