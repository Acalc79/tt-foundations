open import Foundation
open import Function.Metric using (DistanceFunction)
open import Relation.Binary.Core using (Rel)

module Sequence.Limit
  {a b c d}{A : Set a}{B : Set b}
  (∣_-_∣ : DistanceFunction A B)(0< : B → Set c)(_<_ : Rel B d)
  where

import Data.Nat as ℕ
open import Sequence.Core

_tends-to_ : (a : Sequence A)(l : A) → Set _
_tends-to_ a l = ∀ ε → 0< ε → Σ[ N ∈ ℕ ] ∀ n → n ℕ.≤ N → ∣ a n - l ∣ < ε
