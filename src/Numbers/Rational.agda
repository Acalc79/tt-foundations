{-# OPTIONS --without-K --safe #-}
module Numbers.Rational where

open import Data.Rational public
open import Data.Rational.Properties public
open import Data.Rational.Solver public
  renaming (module +-*-Solver to ℚ-Solver)
open import Numbers.Rational.Properties public
open import Numbers.Rational.Extra public

module ℚᵘ where
  open import Data.Rational.Unnormalised public
  open import Data.Rational.Unnormalised.Properties public
