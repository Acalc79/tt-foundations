module Numbers.Nat where

open import Data.Nat renaming (suc to _+1) public
open import Data.Nat.Properties public
open import Numbers.Nat.Properties public

pattern _+2 x = x +1 +1
pattern _+3 x = x +2 +1
pattern _+4 x = x +3 +1
pattern _+5 x = x +4 +1
