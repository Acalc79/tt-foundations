module Numbers.Rational.IntermediateValueTheorem where

import Data.Nat as ℕ
import Data.Nat.Properties as ℕ
open import Foundation hiding (_⊔_)
import Numbers.Integer as ℤ
open import Numbers.Rational
open import Numbers.Rational.Literals
open import Numbers.Rational.Function

module _
  {f p₀ p₁ q}(cont-f : cont f)(fp₀<q : f p₀ < q)(q<fp₁ : q < f p₁)
  {ε}(ε>0 : Positive ε)
  where
  open ℚ-Solver
  
  private
    ε≢0 = NonZero-≢0 ε $ >-nonZero $ positive⁻¹ {ε} ε>0
    -- 2ⁿ≢0 = λ n → NonZero-≢0 (2 ^ n) $ ^-nonZero n _
    
    -- proof due to Matthew Frank in https://arxiv.org/abs/1701.02227
    a b c d s : (n : ℕ) → ℚ
    a 0 = p₀
    a (ℕ.suc n) = c n - s n
    b 0 = p₁
    b (ℕ.suc n) = b n - s n
    c n = avg (a n)(b n)
    d n = 0 ⊔ 1 ⊓ ((f (c n) - q) ÷ ε) {ε≢0}
    s n = d n * ((b n - a n) * ½)

    a-b : ∀ n → b n - a n ≡ (p₁ - p₀) * ½ ^ n
    a-b 0 = sym $ *-identityʳ (p₁ - p₀)
    a-b n+1@(ℕ.suc n) = begin
      b n - s n - (c n - s n)
        ≡⟨ solve 3 (λ a b s → b :- s :- ((a :+ b) :* con ½ :- s) :=
                              (b :- a) :* con ½)
                   (λ {_}{_}{_} → refl)(a n)(b n)(s n) ⟩
      (b n - a n) * ½         ≡⟨ cong (_* ½) $ a-b n ⟩
      (p₁ - p₀) * ½ ^ n * ½   ≡⟨ *-assoc (p₁ - p₀) (½ ^ n) ½ ⟩
      (p₁ - p₀) * (½ ^ n * ½) ≡⟨ cong ((p₁ - p₀) *_) $ *-comm (½ ^ n) ½ ⟩
      (p₁ - p₀) * (½ * ½ ^ n) ≡˘⟨ cong ((p₁ - p₀) *_) $ ^suc ½ n ⟩
      (p₁ - p₀) * ½ ^ n+1 ∎
      where
      open ≡-Reasoning

    lemma : ∀ m →
      (∃[ j ] j ℕ.≤ m × ∣ f (c j) - q ∣ < ε) ⊎ f (a m) < q × f (b m) > q
    lemma 0 = inj₂ $ fp₀<q , q<fp₁
    lemma (ℕ.suc m) = let f' = f (c m) - q in case lemma m of λ
      { (inj₁ (j , j≤m , fcj-q<ε)) → inj₁ $ j , ℕ.≤-step j≤m , fcj-q<ε
      ; (inj₂ (fam<q , fbm>q)) → case f' ≤? - ε of λ
        { (no f'≰-ε) → case ε ≤? f' of λ
          { (no ε≰f') → inj₁ $
            m ,
            ℕ.n≤1+n m ,
            ∣p∣<q f' ε ε>0 (≰⇒> f'≰-ε)(≰⇒> ε≰f')
          ; (yes ε≤f') → let
            open ≤-Reasoning
            dm≡1 : d m ≡ 1
            dm≡1 = cong (0 ⊔_) $
              p≤q⇒p⊓q≡p {1}{(f' ÷ ε) {ε≢0}} $
              *-cancelʳ-≤ ε ε>0 $ begin
              1 * ε  ≡⟨ *-identityˡ ε ⟩
              ε      ≤⟨ ε≤f' ⟩
              f'     ≡˘⟨ *-identityʳ f'  ⟩
              f' * 1 ≡˘⟨ cong (f' *_) $ *-inverseˡ ε {ε≢0} ⟩
              f' * ((1/ ε) {ε≢0} * ε)
                ≡˘⟨ *-assoc f' ((1/ ε) {ε≢0}) ε ⟩
              f' * (1/ ε) {ε≢0} * ε ∎
            sm≡b-a/2 : s m ≡ (b m - a m) * ½
            sm≡b-a/2 = begin-equality
              s m ≡⟨⟩
              d m * ((b m - a m) * ½)
                ≡⟨ cong (_* ((b m - a m) * ½)) dm≡1 ⟩
              1 * ((b m - a m) * ½) ≡⟨ *-identityˡ ((b m - a m) * ½) ⟩
              (b m - a m) * ½ ∎
            bm+1≡cm : c m ≡ b (ℕ.suc m)
            bm+1≡cm = begin-equality
              c m ≡⟨⟩
              (a m + b m) * ½
                ≡⟨ solve 2 (λ b a → (a :+ b) :* con ½ :=
                                     b :- (b :- a) :* con ½)
                         (λ {_}{_} → refl)(b m)(a m) ⟩
              b m - (b m - a m) * ½
                ≡˘⟨ cong (λ sm → b m - sm) sm≡b-a/2 ⟩
              b m - s m ≡⟨⟩
              b (ℕ.suc m) ∎
            am+1≡am : a m ≡ a (ℕ.suc m)
            am+1≡am = begin-equality
              a m ≡⟨ solve 2 (λ a b → a := (a :+ b) :* con ½ :- (b :- a) :* con ½)
                       (λ {_}{_} → refl)(a m)(b m) ⟩
              (a m + b m) * ½ - (b m - a m) * ½
                ≡˘⟨ cong (λ sm → c m - sm) sm≡b-a/2 ⟩
              (a m + b m) * ½ - s m ≡⟨⟩
              a (ℕ.suc m) ∎
          in inj₂ $
          subst (λ o → f o < q) am+1≡am fam<q ,
          subst (λ o → f o > q) bm+1≡cm (
            0<q-p⇒p<q $ <-≤-trans (positive⁻¹ {ε} ε>0) ε≤f')} 
        ; (yes f'≤-ε) → let
          open ≤-Reasoning
          f'≤0 : (f' ÷ ε) {ε≢0} ≤ 0
          f'≤0 = *-cancelʳ-≤ ε ε>0 $ begin
            f' * (1/ ε) {ε≢0} * ε    ≡⟨ *-assoc f' ((1/ ε) {ε≢0}) ε ⟩
            f' * ((1/ ε) {ε≢0} * ε) ≡⟨ cong (f' *_) $ *-inverseˡ ε {ε≢0} ⟩
            f' * 1 ≡⟨ *-identityʳ f' ⟩
            f' ≤⟨ f'≤-ε ⟩
            - ε <⟨ neg-antimono-< $ positive⁻¹ ε>0 ⟩
            0 ≡˘⟨ *-zeroˡ ε ⟩
            0 * ε ∎
          dm≡0 : d m ≡ 0
          dm≡0 = begin-equality
            d m ≡⟨⟩
            0 ⊔ 1 ⊓ (f' ÷ ε) {ε≢0}
              ≡⟨ cong (0 ⊔_) $ p≥q⇒p⊓q≡q {1}{(f' ÷ ε) {ε≢0}} $
                 ≤-trans f'≤0 (*≤* (ℤ.+≤+ ℕ.z≤n)) ⟩
            0 ⊔ (f' ÷ ε) {ε≢0} ≡⟨ p≥q⇒p⊔q≡p f'≤0 ⟩
            0 ∎            
          sm≡0 : s m ≡ 0
          sm≡0 = begin-equality
            s m                     ≡⟨⟩
            d m * ((b m - a m) * ½) ≡⟨ cong (_* ((b m - a m) * ½)) dm≡0 ⟩
            0 * ((b m - a m) * ½)   ≡⟨ *-zeroˡ ((b m - a m) * ½) ⟩
            0 ∎
          bm≡bm+1 : b m ≡ b (ℕ.suc m)
          bm≡bm+1 = begin-equality
            b m       ≡˘⟨ +-identityʳ (b m) ⟩
            b m - 0   ≡˘⟨ cong (λ sm → b m - sm) sm≡0 ⟩
            b m - s m ≡⟨⟩
            b (ℕ.suc m) ∎
          cm≡am+1 : c m ≡ a (ℕ.suc m)
          cm≡am+1 = begin-equality
            c m       ≡˘⟨ +-identityʳ (c m) ⟩
            c m - 0   ≡˘⟨ cong (λ sm → c m - sm) sm≡0 ⟩
            c m - s m ≡⟨⟩
            a (ℕ.suc m) ∎
          in inj₂ $
          subst (λ o → f o < q) cm≡am+1 (p-q<0⇒p<q $
            ≤-<-trans f'≤-ε $ neg-antimono-< $ positive⁻¹ ε>0) ,
          subst (λ o → f o > q) bm≡bm+1 fbm>q}}

  intermediate-value-theorem : ∃[ c ] p₀ < c × c < p₁ × ∣ f c - q ∣ < ε
  intermediate-value-theorem = {!cont-f!}
