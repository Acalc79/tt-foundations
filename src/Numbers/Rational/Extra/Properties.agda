module Numbers.Rational.Extra.Properties where

open import Foundation

open import Data.Nat as ℕ using (ℕ; suc)
open import Data.Integer.DivMod as ℤ
import Data.Nat.Properties as ℕ
open import Data.Rational
open import Data.Rational.Properties
import Data.Rational.Unnormalised as ℚᵘ
import Data.Rational.Unnormalised.Properties as ℚᵘ
open import Numbers.Rational.Properties
open import Numbers.Rational.Literals
open import Numbers.Rational.Extra.Base as ℚ hiding (suc)
open import Numbers.Integer as ℤ using (ℤ)
open import Numbers.Integer.Literals

private
  variable
    p q : ℚ
    a b c : ℤ

----------------------------------------------------------------------
-- properties of injections ℤ→ℚ and ℕ→ℚ

ℤ→ℚ-mono-< : ℤ→ℚ Preserves ℤ._<_ ⟶ _<_
ℤ→ℚ-mono-< = *<* ∘ ℤ.*-monoʳ-<-pos 0

ℤ→ℚ-mono-≤ : ℤ→ℚ Preserves ℤ._≤_ ⟶ _≤_
ℤ→ℚ-mono-≤ = *≤* ∘ ℤ.*-monoʳ-≤-pos 0

module _ where
  open ≤-Reasoning
  
  ----------------------------------------------------------------------
  -- properties of _^_
  
  0^_ : ∀ n → 0 ^ suc n ≡ 0
  0^ 0 = refl
  0^ suc n = *-zeroˡ (0 ^ suc n)
  
  1^_ : ∀ n → 1 ^ n ≡ 1
  1^ 0 = refl
  1^ 1 = refl
  1^ suc (suc n) = begin-equality
    1 * 1 ^ suc n ≡⟨ *-identityˡ (1 ^ suc n) ⟩
    1 ^ suc n ≡⟨ 1^ suc n ⟩
    1 ∎
  
  ^suc : ∀ p n → p ^ suc n ≡ p * p ^ n
  ^suc p 0 = sym $ *-identityʳ p
  ^suc p (suc n) = refl
  
  ----------------------------------------------------------------------
  -- properties of _^_ and _+_
  
  ^-+ : ∀ p m n → p ^ (m ℕ.+ n) ≡ p ^ m * p ^ n
  ^-+ p 0 n = sym $ *-identityˡ (p ^ n)
  ^-+ p 1 n = ^suc p n
  ^-+ p (suc (suc m)) n = begin-equality
    p * p ^ (suc m ℕ.+ n) ≡⟨ cong (p *_) $ ^-+ p (suc m) n ⟩
    p * (p ^ suc m * p ^ n) ≡˘⟨ *-assoc p (p ^ suc m) (p ^ n) ⟩
    p * p ^ suc m * p ^ n ∎
  
  *-^ : ∀ p q m → (p * q) ^ m ≡ p ^ m * q ^ m
  *-^ p q 0 = sym $ *-identityˡ 1
  *-^ p q 1 = refl
  *-^ p q (suc m@(suc _)) = begin-equality
    p * q * (p * q) ^ m ≡⟨ cong (λ x → p * q * x) $ *-^ p q m ⟩
    p * q * (p ^ m * q ^ m) ≡˘⟨ *-assoc (p * q) (p ^ m) (q ^ m) ⟩
    p * q * p ^ m * q ^ m ≡⟨ cong (_* q ^ m) $ *-assoc p q (p ^ m) ⟩
    p * (q * p ^ m) * q ^ m ≡⟨ cong (λ x → p * x * q ^ m) $ *-comm q (p ^ m) ⟩
    p * (p ^ m * q) * q ^ m ≡˘⟨ cong (_* q ^ m) $ *-assoc p (p ^ m) q ⟩
    p * p ^ m * q * q ^ m ≡⟨ *-assoc (p ^ suc m) q (q ^ m) ⟩
    p * p ^ m * (q * q ^ m) ∎
  
  ^-* : ∀ p m n → p ^ (m ℕ.* n) ≡ (p ^ m) ^ n
  ^-* p 0 n = sym $ 1^ n
  ^-* p (suc m) n = begin-equality
    p ^ (n ℕ.+ m ℕ.* n) ≡⟨ ^-+ p n (m ℕ.* n) ⟩
    p ^ n * p ^ (m ℕ.* n) ≡⟨ cong (p ^ n *_) $ ^-* p m n ⟩
    p ^ n * p ^ m ^ n ≡˘⟨ *-^ p (p ^ m) n ⟩
    (p * p ^ m) ^ n ≡˘⟨ cong (_^ n) $ ^suc p m ⟩
    p ^ suc m ^ n ∎
  
  ----------------------------------------------------------------------
  -- properties of _^_ and NonZero
  
  -- ^-nonZero : ∀ {p} n → NonZero p → NonZero (p ^ n)
  -- defined in Rational.Extra
  
  ----------------------------------------------------------------------
  -- properties of _^_ and NonNegative
  
  ^-nonNeg : ∀ {p} n → NonNegative p → NonNegative (p ^ n)
  ^-nonNeg 0 _ = _
  ^-nonNeg 1 p≥0 = p≥0
  ^-nonNeg {p} (suc (suc n)) p≥0 =
    nonNeg-*-nonNeg {p}{p ^ suc n} p≥0 (^-nonNeg (suc n) p≥0)
  
  ²-nonNeg : ∀ p → NonNegative (p ²)
  ²-nonNeg p = case 0 ≤? p of λ
    { (no p≱0) → let nneg‿-p : NonNegative (- p)
                     nneg‿-p = nonNegative $ <⇒≤ $ neg-antimono-< $ ≰⇒> p≱0
                     [-p]²≡p² : (- p) ² ≡ p ²
                     [-p]²≡p² = begin-equality
                       (- p)² ≡˘⟨ neg-distribˡ-* p (- p) ⟩
                       - (p * (- p)) ≡˘⟨ cong (-_) $ neg-distribʳ-* p p ⟩
                       - - p ² ≡⟨ neg-involutive (p ²) ⟩
                       p ² ∎
                 in subst NonNegative [-p]²≡p² $
                      nonNeg-*-nonNeg { - p}{ - p} nneg‿-p nneg‿-p  
    ; (yes p≥0) → let nneg-p = nonNegative p≥0
                  in nonNeg-*-nonNeg {p}{p} nneg-p nneg-p}
  
  ^-even-nonNeg : ∀ p n → NonNegative (p ^ (2 ℕ.* n))
  ^-even-nonNeg p n = subst NonNegative change $ ²-nonNeg (p ^ n)
    where change : p ^ n ² ≡ p ^ (2 ℕ.* n)
          change = begin-equality
            p ^ n ² ≡˘⟨ ^-* p n 2 ⟩
            p ^ (n ℕ.* 2) ≡⟨ cong (p ^_) $ ℕ.*-comm n 2 ⟩
            p ^ (2 ℕ.* n) ∎
  
  ----------------------------------------------------------------------
  -- properties of _^_ and Positive
  
  ^-pos : ∀ {p} n → Positive p → Positive (p ^ n)
  ^-pos 0 _ = _
  ^-pos 1 p>0 = p>0
  ^-pos {p} (suc (suc n)) p>0 =
    pos-*-pos {p}{p ^ suc n} p>0 (^-pos (suc n) p>0)
  
  
  ²-pos : ∀ {p} → NonZero p → Positive (p ²)
  ²-pos {p} p≠0 = case p ² ≟ 0 of λ
    { (no p²≠0) → positive {p ²} $ ≰⇒> λ p²≤0 → p²≠0 $
                    ≤-antisym p²≤0 $ nonNegative⁻¹ $ ²-nonNeg p
    ; (yes p²=0) → ⊥-elim $ nonZero⁻¹ p≠0 $ [ id , id ]′ (p*q≡0⇒p≡0∨q≡0 p p p²=0)}
  
  ^-even-pos : ∀ {p} n → NonZero p → Positive (p ^ (2 ℕ.* n))
  ^-even-pos {p} n p≠0 = subst Positive change $ ²-pos {p ^ n} $ ^-nonZero n p≠0
    where change : p ^ n ² ≡ p ^ (2 ℕ.* n)
          change = begin-equality
            p ^ n ² ≡˘⟨ ^-* p n 2 ⟩
            p ^ (n ℕ.* 2) ≡⟨ cong (p ^_) $ ℕ.*-comm n 2 ⟩
            p ^ (2 ℕ.* n) ∎
  
  ----------------------------------------------------------------------
  -- properties of _^_ and _≤_
  
  ^-≤ : ∀{p} n → 1 ≤ p → p ≤ p ^ suc n
  ^-≤ 0 1≤p = ≤-refl
  ^-≤ {p} (suc n) 1≤p = begin
    p ≡˘⟨ *-identityˡ p ⟩
    1 * p ≤⟨ *-monoʳ-≤-nonNeg p p≥0 {1}{p} 1≤p ⟩
    p * p ≤⟨ *-monoˡ-≤-nonNeg p p≥0 {p}{p ^ suc n} $ ^-≤ n 1≤p ⟩
    p * p ^ suc n ∎
    where p≥0 : NonNegative p
          p≥0 = nonNegative $ ≤-trans (*≤* $ ℤ.+≤+ ℕ.z≤n) 1≤p
  
  ----------------------------------------------------------------------
  -- properties of _^_ and _<_
  
  ^-mono-<-nonNeg : ∀{p q}(0≤p : NonNegative p) n
      → ----------------------------------------------------------------------
      p < q → p ^ suc n < q ^ suc n
  ^-mono-<-nonNeg 0≤p 0 p<q = p<q
  ^-mono-<-nonNeg {p}{q} 0≤p (suc n) p<q = begin-strict
    p * p ^ suc n ≤⟨ *-monoʳ-≤-nonNeg (p ^ suc n) (^-nonNeg (suc n) 0≤p) $ <⇒≤ p<q ⟩
    q * p ^ suc n <⟨ *-monoʳ-<-pos q 0<q (^-mono-<-nonNeg 0≤p n p<q) ⟩
    q * q ^ suc n ∎
    where
      0<q : Positive q
      0<q = positive {q} $ begin-strict
        0 ≤⟨ nonNegative⁻¹ 0≤p ⟩
        p <⟨ p<q ⟩
        q ∎

^-cancel-<-nonNeg : ∀{p q}(0≤q : NonNegative q) n
    → ----------------------------------------------------------------------
    p ^ suc n < q ^ suc n → p < q
^-cancel-<-nonNeg {p} {q} 0≤q n p^n<q^n = case <-cmp p q of λ
  { (tri< p<q _ _) → p<q
  ; (tri≈ _ refl _) → ⊥-elim $ <-irrefl refl p^n<q^n
  ; (tri> _ _ q<p) → ⊥-elim $ <-asym p^n<q^n $ ^-mono-<-nonNeg 0≤q n q<p}

------------------------------------------------------------------------
-- properties of ⌈_⌉, ⌊_⌋ and _≤_

p≤⌊p⌋ : ℤ→ℚ ⌊ p ⌋ ≤ p
p≤⌊p⌋ {p} = *≤* $ begin
  (↥ ℤ→ℚ ⌊ p ⌋) ℤ.* (↧ p)    ≡⟨⟩
  (↥ p) divℕ (↧ₙ p) ℤ.* (↧ p) ≤⟨ ℤ.[n/ℕd]*d≤n (↥ p)(↧ₙ p) ⟩
  ↥ p                          ≡˘⟨ ℤ.*-identityʳ (↥ p) ⟩
  ↥ p ℤ.* 1 ∎
  where open ℤ.≤-Reasoning

p≤⌈p⌉ : p ≤ ℤ→ℚ ⌈ p ⌉
p≤⌈p⌉ {p@(mkℚ _ 0 _)} = ≤-refl
p≤⌈p⌉ {p@(mkℚ n (suc d-2) _)} = *≤* $ begin
  n ℤ.* 1 ≡⟨ ℤ.*-identityʳ n ⟩
  n <⟨ n<s[n/ℕd]*d n d ⟩
  ℤ.suc ⌊ p ⌋ ℤ.* ℤ.+ d ∎
  where
  d = 2 ℕ.+ d-2
  open ℤ.≤-Reasoning
