module Numbers.Rational.Extra.Average.Properties where

open import Data.Rational
open import Data.Rational.Properties
open import Data.Rational.Solver renaming (module +-*-Solver to ℚ-Solver)
open import Foundation
open import Numbers.Rational.Extra.Average
open import Numbers.Rational.Literals
open import Numbers.Rational.Properties

private variable
  p q : ℚ

module _ where
  open ≤-Reasoning
  open ℚ-Solver

  ----------------------------------------------------------------------
  -- properties of wavg

  wavg-swap : ∀ p w q → wavg p w q ≡ wavg q (1 - w) p
  wavg-swap p w q = solve 3
    (λ p w q → p :* w :+ q :* (con 1 :- w) :=
               q :* (con 1 :- w) :+ p :* (con 1 :- (con 1 :- w)))
    (λ {_}{_}{_} → refl) p w q
    
  wavg<wavg : p < q → ∀{w₁ w₂} → w₂ < w₁ → wavg p w₁ q < wavg p w₂ q
  wavg<wavg {p}{q} p<q {w₁}{w₂} w₂<w₁ = begin-strict
    wavg p w₁ q ≡⟨⟩
    p * w₁ + q * (1 - w₁) ≡⟨ aux w₁ ⟩
    (q - p) * - w₁ + q
      <⟨ +-monoˡ-< q $
         *-monoʳ-<-pos (q - p) (positive $ p<q⇒0<q-p p<q) $
         neg-antimono-< w₂<w₁ ⟩
    (q - p) * - w₂ + q    ≡˘⟨ aux w₂ ⟩
    p * w₂ + q * (1 - w₂) ≡⟨⟩
    wavg p w₂ q ∎
    where
    aux : ∀ w → p * w + q * (1 - w) ≡ (q - p) * - w + q
    aux = solve 3
      (λ p q w → p :* w :+ q :* (con 1 :- w) := (q :- p) :* :- w :+ q)
      (λ {_}{_}{_} → refl) p q

  module _ w {w>0 : Positive w}{1-w>0 : Positive (1 - w)} (p<q : p < q)
    where
    <-wavgˡ : p < wavg p w q
    <-wavgˡ = begin-strict
      p ≡⟨ solve 2 (λ p w → p := p :* w :+ p :* (con 1 :- w))
                 (λ {_}{_} → refl) p w ⟩
      p * w + p * (1 - w)
        <⟨ +-monoʳ-< (p * w) (*-monoˡ-<-pos (1 - w) 1-w>0 p<q) ⟩
      p * w + q * (1 - w) ≡⟨⟩
      wavg p w q ∎
    
    wavg-<ʳ : wavg p w q < q
    wavg-<ʳ = begin-strict
      wavg p w q ≡⟨⟩
      p * w + q * (1 - w)
        <⟨ +-monoˡ-< (q * (1 - w)) (*-monoˡ-<-pos w w>0 p<q) ⟩
      q * w + q * (1 - w)
        ≡⟨ solve 2 (λ q w → q :* w :+ q :* (con 1 :- w) := q)
                 (λ {_}{_} → refl) q w ⟩
      q ∎ 
    
  module _ w {w>0 : Positive w}{1-w>0 : Positive (1 - w)} (p<q : p < q)
    where
    private
      w≡1-[1-w] : w ≡ 1 - (1 - w)
      w≡1-[1-w] = solve 1 (λ w → w := con 1 :- (con 1 :- w))
                        (λ {_} → refl) w

    <-wavgʳ : p < wavg q w p
    <-wavgʳ = subst (p <_) (sym (wavg-swap q w p))
      (<-wavgˡ (1 - w) {1-w>0}{subst Positive w≡1-[1-w] w>0} p<q)
    
    wavg-<ˡ : wavg q w p < q
    wavg-<ˡ = subst (_< q) (sym (wavg-swap q w p))
      (wavg-<ʳ (1 - w) {1-w>0} {subst Positive w≡1-[1-w] w>0} p<q)

  ----------------------------------------------------------------------
  -- properties of avg

  avg-comm : ∀ p q → avg p q ≡ avg q p
  avg-comm p q = wavg-swap p ½ q
  
  <-avgˡ : p < q → p < avg p q
  <-avgˡ p<q = <-wavgˡ ½ p<q
  
  <-avgʳ : p < q → p < avg q p
  <-avgʳ p<q = <-wavgʳ ½ p<q
  
  avg-<ʳ : p < q → avg p q < q
  avg-<ʳ p<q =  wavg-<ʳ ½ p<q
  
  avg-<ˡ : p < q → avg q p < q
  avg-<ˡ p<q = wavg-<ˡ ½ p<q
