module Numbers.Rational.Extra.Log where

open import Data.Nat as ℕ using (ℕ; suc)
open import Data.Rational
open import Data.Rational.Properties
open import Foundation
open import Numbers.Rational.Literals

⌊log2_⌋ : ℚ → ℕ
⌊log2 q ⌋ = case q ≤? 1 of λ
  { (no q≰1) → {!!}
  ; (yes q≤1) → 0}
