module Numbers.Rational.Extra.Base where

open import Foundation 

open import Data.Nat as ℕ using (ℕ; zero)
open import Data.Nat.Coprimality as ℕ
open import Numbers.Integer as ℤ using (ℤ; +_; -[1+_]; ℕ→ℤ)
open import Data.Integer.DivMod as ℤ
open import Data.Rational
open import Data.Rational.Properties
open import Data.Rational.Unnormalised as ℚᵘ using (ℚᵘ)
open import Numbers.Rational.Literals
open import Numbers.Rational.Properties

ℤ→ℚ : ℤ → ℚ
ℤ→ℚ a = mkℚ a 0 (ℕ.sym $ 1-coprimeTo ℤ.∣ a ∣)

ℕ→ℚ : ℕ → ℚ
ℕ→ℚ = ℤ→ℚ ∘ ℕ→ℤ

suc : ℚ → ℚ
suc p = 1 + p

infixl 9 _^_ _^`_ _^^_ _²

_^_ : (a : ℚ)(n : ℕ) → ℚ
a ^ 0 = 1
a ^ 1 = a
a ^ ℕ.suc (ℕ.suc n) = a * (a ^ ℕ.suc n)

_² : (a : ℚ) → ℚ
_² = _^ 2

_^`_ : (a : ℚ)(n : ℕ) → ℚ
a ^` 0 = 1
a ^` ℕ.suc n = a * (a ^ n)

^-nonZero : ∀{a} (n : ℕ)(a≢0 : NonZero a) → NonZero (a ^ n)
^-nonZero 0 a≢0 = _
^-nonZero 1 a≢0 = a≢0
^-nonZero {a} (ℕ.suc (ℕ.suc n)) a≢0 =
  nonZero-*-nonZero a (a ^ ℕ.suc n) a≢0 $ ^-nonZero (ℕ.suc n) a≢0

_^^_ : (a : ℚ)(n : ℤ) → .{a≢0 : NonZero a} → ℚ
a ^^ (+ n) = a ^ n
_^^_ a -[1+ n ] {a≢0} = (1/ a ^ ℕ.suc n)
  {NonZero-≢0 (a ^ ℕ.suc n) $ ^-nonZero (ℕ.suc n) a≢0}

⌊_⌋ : (p : ℚ) → ℤ
⌊ p ⌋ = (↥ p) divℕ (↧ₙ p)

⌈_⌉ : (p : ℚ) → ℤ
⌈ mkℚ i 0 _ ⌉ = i
⌈ p@(mkℚ _ (ℕ.suc _) _) ⌉ = ℤ.suc ⌊ p ⌋
