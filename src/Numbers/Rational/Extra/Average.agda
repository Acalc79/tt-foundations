module Numbers.Rational.Extra.Average where

open import Foundation 

open import Data.Rational
open import Numbers.Rational.Literals

wavg : (a wa b : ℚ) → ℚ
wavg a wa b = a * wa + b * (1 - wa)

avg : (a b : ℚ) → ℚ
avg a b = wavg a ½ b
