module Numbers.Rational.Extra where

open import Numbers.Rational.Extra.Base public
open import Numbers.Rational.Extra.Properties public
open import Numbers.Rational.Extra.Average public
open import Numbers.Rational.Extra.Average.Properties public
