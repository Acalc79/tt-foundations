module Numbers.Rational.Function where

open import Foundation
open import Numbers.Rational

f-mono-<⇒f-mono-≤ :
  {f : ℚ → ℚ} → f Preserves _<_ ⟶ _<_ → f Preserves _≤_ ⟶ _≤_
f-mono-<⇒f-mono-≤ f-mono-< p≤q = case ≤⇒<∨≡ p≤q of λ
  { (inj₁ p<q) → <⇒≤ $ f-mono-< p<q ; (inj₂ refl) → ≤-refl}

f-mono-<⇒f-mono-≤′ :
  {f : ℚ → ℚ} → f Preserves _<_ ⟶ _>_ → f Preserves _≤_ ⟶ _≥_
f-mono-<⇒f-mono-≤′ f-mono-< p≤q = case ≤⇒<∨≡ p≤q of λ
  { (inj₁ p<q) → <⇒≤ $ f-mono-< p<q ; (inj₂ refl) → ≤-refl}

⁻¹-mono-strict-inc : (inv : ℚ ↔ ℚ) → let open Inverse inv in
  f Preserves _<_ ⟶ _<_ → f⁻¹ Preserves _<_ ⟶ _<_
⁻¹-mono-strict-inc inv mono-f {a}{b} a<b = case <-cmp (f⁻¹ a)(f⁻¹ b) of λ
  { (tri< f⁻¹a<f⁻¹b _ _) → f⁻¹a<f⁻¹b
  ; (tri≈ _ f⁻¹a≡f⁻¹b _) → ⊥-elim $ flip <-irrefl a<b $ begin-equality
    a ≡˘⟨ inverseˡ a ⟩
    f (f⁻¹ a) ≡⟨ cong f f⁻¹a≡f⁻¹b ⟩
    f (f⁻¹ b) ≡⟨ inverseˡ b ⟩
    b ∎
  ; (tri> _ _ f⁻¹b<f⁻¹a) → ⊥-elim $ <-asym a<b $ begin-strict
    b ≡˘⟨ inverseˡ b ⟩
    f (f⁻¹ b) <⟨ mono-f f⁻¹b<f⁻¹a ⟩
    f (f⁻¹ a) ≡⟨ inverseˡ a ⟩
    a ∎}
  where
    open Inverse inv
    open ≤-Reasoning

cont-at : (f : ℚ → ℚ)(x₀ : ℚ) → Set
cont-at f x₀ = ∀ ε → Positive ε →
  ∃[ δ ] Positive δ × (∀ x → ∣ x₀ - x ∣ < δ → ∣ f x₀ - f x ∣ < ε)

cont : (f : ℚ → ℚ) → Set
cont f = ∀ x → cont-at f x

-- intermediate value property

ivp : (f : ℚ → ℚ) → Set
ivp f = ∀{p₀ p₁ q}
        (fp₀<q : f p₀ < q)(q<fp₁ : q < f p₁){ε}(ε>0 : Positive ε) →
        ∃[ c ] p₀ < c × c < p₁ × ∣ f c - q ∣ < ε
