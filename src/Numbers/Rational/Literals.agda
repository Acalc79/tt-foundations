module Numbers.Rational.Literals where

open import Data.Unit using (tt) public
open import Data.Rational.Literals
open import Agda.Builtin.FromNat using (fromNat) public
open import Agda.Builtin.FromNeg using (fromNeg) public
instance
  Numberℚ = number
  Negativeℚ = negative
