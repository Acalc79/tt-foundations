module Numbers.Rational.Properties where

open import Foundation

open import Data.Rational
open import Data.Rational.Properties
open import Data.Rational.Unnormalised as ℚᵘ
  renaming (_≤_ to _≤ᵘ_; _<_ to _<ᵘ_) using (ℚᵘ; *<*; _≢0)
import Data.Rational.Unnormalised.Properties as ℚᵘ
open import Numbers.Rational.Literals
open import Relation.Nullary.Decidable

open import Algebra.Morphism
open Definitions ℚ ℚᵘ ℚᵘ._≃_
open import Algebra.Definitions {A = ℚ} _≡_
open import Algebra.Properties.Group +-0-group
open import Relation.Binary

open import Data.Nat as ℕ using (ℕ; suc)
open import Numbers.Integer as ℤ using (ℤ; +0; -[1+_])
open import Numbers.Integer.Literals

private
  variable
    p q : ℚ
    i j k : ℤ

open import Function.Reasoning

-- many properties inherited/copied from Unnormalized

------------------------------------------------------------------------
-- Numerator and denominator inequality
------------------------------------------------------------------------

0≤p⇒0≤↥p : 0 ≤ p → 0 ℤ.≤ ↥ p
0≤p⇒0≤↥p = ℤ.nonNegative⁻¹ ∘ nonNegative

0<p⇒0<↥p : 0 < p → 0 ℤ.< ↥ p
0<p⇒0<↥p = ℤ.positive⁻¹ ∘ positive

------------------------------------------------------------------------
-- Properties of normalize
------------------------------------------------------------------------

↥-normalize*den≡↧-normalize*nom : ∀ m n {n≢0 : n ≢0} →
  ↥ normalize m n {n≢0} ℤ.* ℤ.+ n ≡ (ℤ.+ m) ℤ.* ↧ normalize m n {n≢0}
↥-normalize*den≡↧-normalize*nom m n {n≢0} =
  ℤ.*-cancelʳ-≡ (↥ norm ℤ.* n')(m' ℤ.* ↧ norm) g
    (λ g≡0 → case ℤ.gcd[i,j]≡0⇒j≡0 {m'}{n'} g≡0 of λ {refl → ⊥-elim n≢0}) $
    begin
      ↥ norm ℤ.* n' ℤ.* g ≡⟨ solve 3 (λ a b c → a :* b :* c := a :* c :* b)
                                     (λ {_}{_}{_} → refl) (↥ norm) n' g ⟩
                   -- TODO: figure out why implicit lambda is necessary
      ↥ norm ℤ.* g ℤ.* n' ≡⟨ cong (ℤ._* n') $ ↥-normalize m n {n≢0} ⟩
      m' ℤ.* n' ≡⟨ ℤ.*-comm m' n' ⟩
      n' ℤ.* m' ≡˘⟨ cong (ℤ._* m') $ ↧-normalize m n {n≢0} ⟩
      ↧ norm ℤ.* g ℤ.* m' ≡⟨ solve 3 (λ n g m → n :* g :* m := m :* n :* g)
                                      (λ {_}{_}{_} → refl) (↧ norm) g m' ⟩
      m' ℤ.* ↧ norm ℤ.* g ∎
  where norm = normalize m n {n≢0}
        m' = ℤ.+ m
        n' = ℤ.+ n
        g = ℤ.gcd m' n'
        open ≡-Reasoning
        open import Data.Integer.Solver using (module +-*-Solver)
        open +-*-Solver

------------------------------------------------------------------------
-- Properties of Positive/Negative/NonPositive/NonNegative predicates
------------------------------------------------------------------------

positive⇒nonNegative : ∀ {q} → Positive q → NonNegative q
positive⇒nonNegative {mkℚ +0       _ _} _ = _
positive⇒nonNegative {mkℚ +[1+ n ] _ _} _ = _

negative⇒nonPositive : ∀ {q} → Negative q → NonPositive q
negative⇒nonPositive {mkℚ +0       _ _} _ = _
negative⇒nonPositive {mkℚ -[1+ n ] _ _} _ = _

------------------------------------------------------------------------
-- Relationship between other operators

≤⇒<∨≡ : ∀{p q} → p ≤ q → p < q ⊎ p ≡ q
≤⇒<∨≡ {p}{q} p≤q = case p <? q of λ
  { (no p≮q) → inj₂ $ ≤-antisym p≤q $ ≮⇒≥ p≮q
  ; (yes p<q) → inj₁ p<q}

<⇒≱ : _<_ ⇒ _≱_
<⇒≱ (*<* x<y) = ℤ.<⇒≱ x<y ∘ drop-*≤*

------------------------------------------------------------------------
-- Properties of _/_

module _ where
  open ≡-Reasoning
  private
    u = toℚᵘ
    u↑ = ℚᵘ.↥_
    u↓ = ℚᵘ.↧_
    u- = ℚᵘ.-_

    aux : ∀ m n {n≢0 : n ≢0} →
          u↑ (u (normalize m n {n≢0})) ℤ.* u↓ ((ℤ.+ m ℚᵘ./ n) {n≢0}) ≡
          u↑ ((ℤ.+ m ℚᵘ./ n) {n≢0}) ℤ.* u↓ (u (normalize m n {n≢0}))
    aux m n@(suc _) = begin
      u↑ (u (normalize m n)) ℤ.* u↓ (ℤ.+ m ℚᵘ./ n) ≡⟨⟩
      ↥ normalize m n ℤ.* ℤ.+ n ≡⟨ ↥-normalize*den≡↧-normalize*nom m n ⟩
      (ℤ.+ m) ℤ.* ↧ normalize m n ≡⟨⟩
      u↑ (ℤ.+ m ℚᵘ./ n) ℤ.* u↓ (u (normalize m n)) ∎

  toℚᵘ-homo-/ : ∀ a n {n≢0 : n ≢0} →
      u ((a / n) {n≢0}) ℚᵘ.≃ (a ℚᵘ./ n) {n≢0}
  toℚᵘ-homo-/ (ℤ.+ m) n@(suc _) = ℚᵘ.*≡* $ aux m n
  toℚᵘ-homo-/ (-[1+ m ]) n@(suc _) = ≃.begin-equality
    u (- normalize (suc m) n) ≃.≈⟨ toℚᵘ-homo‿- $ normalize (suc m) n ⟩
    u- unorm ≃.≈⟨ ℚᵘ.*≡* $ begin
      u↑ (u- unorm) ℤ.* ℤ.+ n ≡⟨⟩
      ℤ.- (u↑ unorm) ℤ.* ℤ.+ n ≡˘⟨ ℤ.neg-distribˡ-* (u↑ unorm)(ℤ.+ n) ⟩
      ℤ.- (u↑ unorm ℤ.* ℤ.+ n) ≡⟨ cong (ℤ.-_) $ aux (suc m) n ⟩
      ℤ.- (+[1+ m ] ℤ.* u↓ unorm) ≡⟨ ℤ.neg-distribˡ-* (+[1+ m ])(u↓ unorm) ⟩
      ℤ.- +[1+ m ] ℤ.* u↓ unorm ≡⟨⟩
      -[1+ m ] ℤ.* u↓ (u- unorm) ∎ ⟩
    -[1+ m ] ℚᵘ./ n ≃.∎
    where
    module ≃ = ℚᵘ.≤-Reasoning
    unorm = u $ normalize (suc m) n

------------------------------------------------------------------------
-- Properties of _+_ and _≤_

≤-steps : ∀ {p q r} → NonNegative r → p ≤ q → p ≤ r + q
≤-steps {p} {q} {r} r≥0 p≤q =
  subst (_≤ r + q) (+-identityˡ p) $
  +-mono-≤ {0}{r}{p}{q} (nonNegative⁻¹ r≥0) p≤q

p≤p+q : ∀ {p q} → NonNegative q → p ≤ p + q
p≤p+q {p} {q} q≥0 =
  subst (_≤ p + q) (+-identityʳ p) $
  +-monoʳ-≤ p (nonNegative⁻¹ {q} q≥0)

p≤q+p : ∀ {p} → NonNegative p → ∀ {q} → q ≤ p + q
p≤q+p {p} p≥0 {q} rewrite +-comm p q = p≤p+q {q}{p} p≥0

module _ where
  open ℚᵘ.≤-Reasoning
  private
    u = toℚᵘ
    u- = ℚᵘ.-_
  
  ------------------------------------------------------------------------
  -- Properties of -_
  ------------------------------------------------------------------------

  ℚᵘ-neg-injective-≡ : ∀{p q} → u- p ≡ u- q → p ≡ q
  ℚᵘ-neg-injective-≡ {ℚᵘ.mkℚᵘ n _} {ℚᵘ.mkℚᵘ n' _} -p≡-q =
    case ℤ.neg-injective (cong (ℚᵘ.↥_) -p≡-q) , cong (ℚᵘ.↧_) -p≡-q of λ
    { (refl , refl) → refl }

  ℚᵘ-neg-injective-≃ : ∀{p q} → u- p ℚᵘ.≃ u- q → p ℚᵘ.≃ q
  ℚᵘ-neg-injective-≃ {p}{q} -p≃-q = begin-equality
    p ≈˘⟨ ℚᵘ.neg-involutive p ⟩
    u- (u- p) ≈⟨ ℚᵘ.-‿cong -p≃-q ⟩
    u- (u- q) ≈⟨ ℚᵘ.neg-involutive q ⟩
    q ∎ 

  neg-involutive : Involutive (-_)
  neg-involutive = ⁻¹-involutive

  ------------------------------------------------------------------------
  -- Properties of _+_ and _<_
  
  <-steps : ∀ {p q r} → NonNegative r → p < q → p < r + q
  <-steps {p} {q} {r} r≥0 p<q =
    subst (_< r + q) (+-identityˡ p) $
    +-mono-≤-< {0}{r}{p}{q} (nonNegative⁻¹ r≥0) p<q
  
  p<p+q : ∀ {p q} → Positive q → p < p + q
  p<p+q {p} {q} q>0 =
    subst (_< p + q) (+-identityʳ p) $
    +-monoʳ-< p (positive⁻¹ {q} q>0)
  
  p<q+p : ∀ {p} → Positive p → ∀ {q} → q < p + q
  p<q+p {p} p≥0 {q} rewrite +-comm p q = p<p+q {q}{p} p≥0

  ------------------------------------------------------------------------
  -- Properties of _*_

  ------------------------------------------------------------------------
  -- Properties of _*_ and _<_
  
  *-mono-<-pos : ∀{p q}(q>0 : Positive q){r s}(r>0 : Positive r)
    → ----------------------------------------------------------------------
    p < q → r < s → p * r < q * s
  *-mono-<-pos {p}{q} q>0 {r}{s} r>0 p<q r<s =
    <-trans (*-monoˡ-<-pos r r>0 p<q)(*-monoʳ-<-pos q q>0 r<s)

  ------------------------------------------------------------------------
  -- properties of _/_ and _<_
  
  /-monoˡ-< : ∀ n (n≢0 : n ℚᵘ.≢0)
    → --------------------------------------------------
    (λ a → (a / n) {n≢0}) Preserves ℤ._<_ ⟶ _<_
  /-monoˡ-< n@(suc n₁) n≢0 {a}{b} a<b = toℚᵘ-cancel-< $ begin-strict
    u (a / n) ≈⟨ toℚᵘ-homo-/ a n ⟩
    a ℚᵘ./ n <⟨ ℚᵘ.*<* $ ℤ.*-monoʳ-<-pos n₁ a<b ⟩
    b ℚᵘ./ n ≈˘⟨ toℚᵘ-homo-/ b n ⟩
    u (b / n) ∎
  
  /-monoʳ-<-pos : ∀{i}(i>0 : ℤ.Positive i){m n}(m≢0 : m ℚᵘ.≢0)(m<n : m ℕ.< n) →
    let n≢0 : n ℚᵘ.≢0
        n≢0 = fromWitnessFalse λ {refl → case m<n of λ ()}
    in --------------------------------------------------
    (i / n) {n≢0} < (i / m) {m≢0}
  /-monoʳ-<-pos {i@(+[1+ i' ])} i>0 {m@(suc _)}{n@(suc _)} m≢0 m<n =
    toℚᵘ-cancel-< $ begin-strict
    u (i / n) ≈⟨ toℚᵘ-homo-/ i n ⟩
    i ℚᵘ./ n <⟨ ℚᵘ.*<* $ ℤ.*-monoˡ-<-pos i' $ ℤ.+<+ m<n ⟩
    i ℚᵘ./ m ≈˘⟨ toℚᵘ-homo-/ i m ⟩
    u (i / m) ∎

module _ where
  open ≡-Reasoning

  ------------------------------------------------------------------------
  -- properties of NonZero
  
  ℚᵘ-nonZero⁻¹ : ∀ p → ℚᵘ.NonZero p → p ℚᵘ.≠ ℚᵘ.0ℚᵘ
  ℚᵘ-nonZero⁻¹ p@(ℚᵘ.mkℚᵘ n d-1) ∣n∣≢0 (ℚᵘ.*≡* n*1≡0*d) with n≡0
    where n≡0 : n ≡ 0
          n≡0 = begin
            n ≡˘⟨ ℤ.*-identityʳ n  ⟩
            n ℤ.* 1 ≡⟨ n*1≡0*d ⟩
            0 ℤ.* ℚᵘ.↧ p ≡⟨ ℤ.*-zeroˡ (ℚᵘ.↧ p) ⟩
            0 ∎
  ℚᵘ-nonZero⁻¹ _ () _ | refl

  nonZero⁻¹ : ∀{p} → NonZero p → p ≢ 0
  nonZero⁻¹ () refl

  NonZero-≢0 : ∀ p → NonZero p → ℤ.∣ ↥ p ∣ ≢0
  NonZero-≢0 p nzp with ℤ.∣ ↥ p ∣
  ... | suc _ = tt
  
  p*q≡0⇒p≡0∨q≡0 : (p q : ℚ) → p * q ≡ 0 → p ≡ 0 ⊎ q ≡ 0
  p*q≡0⇒p≡0∨q≡0 p q pq≡0 = case p ≟ 0 of λ
    { (no p≢0) → let num-p≢0 = NonZero-≢0 p $ ≢-nonZero p≢0
                     1/p = 1/_ p {num-p≢0}
      in inj₂ $ begin
      q ≡˘⟨ *-identityˡ q ⟩
      1 * q ≡˘⟨ cong (_* q) $ *-inverseˡ p {num-p≢0} ⟩
      (1/p * p) * q ≡⟨ *-assoc 1/p p q ⟩
      1/p * (p * q) ≡⟨ cong (1/p *_) pq≡0 ⟩
      1/p * 0 ≡⟨ *-zeroʳ 1/p ⟩
      0 ∎ 
    ; (yes refl) → inj₁ refl}
  
  nonZero-*-nonZero : (a b : ℚ)(a≢0 : NonZero a)(b≢0 : NonZero b) → NonZero (a * b)
  nonZero-*-nonZero a b a≢0 b≢0 = ≢-nonZero {a * b} $
    λ ab≡0 → case p*q≡0⇒p≡0∨q≡0 a b ab≡0 of λ
      { (inj₁ refl) → ⊥-elim a≢0
      ; (inj₂ refl) → ⊥-elim b≢0}

module _ where
  open ≤-Reasoning

  ------------------------------------------------------------------------
  -- properties of NonNegative
  
  nonNeg-*-nonNeg : NonNegative p → NonNegative q → NonNegative (p * q)
  nonNeg-*-nonNeg {p} {q} p≥0 q≥0 = nonNegative $ begin
    0 ≡˘⟨ *-zeroˡ 0 ⟩
    0 * 0 ≤⟨ *-monoʳ-≤-nonNeg 0 _ $ nonNegative⁻¹ {p} p≥0 ⟩
    p * 0 ≤⟨ *-monoˡ-≤-nonNeg p p≥0 $ nonNegative⁻¹ {q} q≥0 ⟩
    p * q ∎

  ------------------------------------------------------------------------
  -- properties of Positive
  
  pos-*-pos : Positive p → Positive q → Positive (p * q)
  pos-*-pos {p}{q} p>0 q>0 = positive $ begin-strict
    0 ≡⟨ *-zeroˡ 0 ⟩
    0 * 0 ≤⟨ *-monoʳ-≤-nonNeg 0 _ $ <⇒≤ $ positive⁻¹ {p} p>0 ⟩
    p * 0 <⟨ *-monoʳ-<-pos p p>0 $ positive⁻¹ {q} q>0 ⟩
    p * q ∎

  ------------------------------------------------------------------------
  -- Properties of _-_

  p<q⇒p-q<0 : ∀ {p q} → p < q → p - q < 0
  p<q⇒p-q<0 {p} {q} p<q = begin-strict
    p - q <⟨ +-monoˡ-< (- q) p<q ⟩
    q - q ≡⟨ +-inverseʳ q ⟩
    0   ∎
  
  p-q<0⇒p<q : ∀ {p q} → p - q < 0 → p < q
  p-q<0⇒p<q {p} {q} p-q<0 = begin-strict
    p             ≡˘⟨ +-identityʳ p ⟩
    p + 0         ≡˘⟨ cong (p +_) $ +-inverseˡ q ⟩
    p + (- q + q) ≡˘⟨ +-assoc p (- q) q ⟩
    (p - q) + q   <⟨ +-monoˡ-< q p-q<0 ⟩
    0 + q         ≡⟨ +-identityˡ q ⟩
    q             ∎
  
  p<q⇒0<q-p : ∀ {p q} → p < q → 0 < q - p
  p<q⇒0<q-p {p} {q} p<q = begin-strict
    0     ≡˘⟨ +-inverseʳ p ⟩
    p - p <⟨ +-monoˡ-< (- p) p<q ⟩
    q - p ∎
  
  0<q-p⇒p<q : ∀ {p q} → 0 < q - p → p < q
  0<q-p⇒p<q {p} {q} 0<p-q = begin-strict
    p             ≡˘⟨ +-identityˡ p ⟩
    0 + p         <⟨ +-monoˡ-< p 0<p-q ⟩
    q - p + p     ≡⟨ +-assoc q (- p) p ⟩
    q + (- p + p) ≡⟨ cong (q +_) $ +-inverseˡ p ⟩
    q + 0         ≡⟨ +-identityʳ q ⟩
    q             ∎

  ------------------------------------------------------------------------
  -- cancellative properties of -_, _+_; _*_ and _<_
  
  neg-cancel-< : ∀ {p q} → - p < - q → q < p
  neg-cancel-< {p}{q} -p<-q = begin-strict
    q ≡˘⟨ neg-involutive q ⟩
    - (- q) <⟨ neg-antimono-< -p<-q ⟩
    - (- p) ≡⟨ neg-involutive p ⟩
    p ∎
  
  +-cancelˡ-< : ∀ {p q} r → r + p < r + q → p < q
  +-cancelˡ-< {p}{q} r r+p<r+q = begin-strict
    p ≡˘⟨ +-identityˡ p ⟩
    0 + p ≡˘⟨ cong (_+ p) $ +-inverseˡ r ⟩
    (- r + r) + p ≡⟨ +-assoc (- r) r p ⟩
    - r + (r + p) <⟨ +-monoʳ-< (- r) r+p<r+q ⟩
    - r + (r + q) ≡˘⟨ +-assoc (- r) r q ⟩
    (- r + r) + q ≡⟨ cong (_+ q) $ +-inverseˡ r ⟩
    0 + q ≡⟨ +-identityˡ q ⟩
    q ∎

  +-cancelʳ-< : ∀{p q} r → p + r < q + r → p < q
  +-cancelʳ-< {p}{q} r rewrite +-comm p r | +-comm q r = +-cancelˡ-< r

  *-cancelˡ-< : ∀{p q} r → Positive r → r * p < r * q → p < q
  *-cancelˡ-< {p}{q} r@(mkℚ (ℤ.+ (suc _)) _ _) r>0 r*p<r*q = begin-strict
    p ≡˘⟨ *-identityˡ p ⟩
    1 * p ≡˘⟨ cong (_* p) $ *-inverseˡ r ⟩
    (1/ r * r) * p ≡⟨ *-assoc (1/ r) r p ⟩
    1/ r * (r * p) <⟨ *-monoʳ-<-pos (1/ r) _ r*p<r*q ⟩
    1/ r * (r * q) ≡˘⟨ *-assoc (1/ r) r q ⟩
    (1/ r * r) * q ≡⟨ cong (_* q) $ *-inverseˡ r ⟩
    1 * q ≡⟨ *-identityˡ q ⟩
    q ∎

  *-cancelʳ-< : ∀{p q} r → Positive r → p * r < q * r → p < q
  *-cancelʳ-< {p}{q} r rewrite *-comm p r | *-comm q r = *-cancelˡ-< r

  ------------------------------------------------------------------------
  -- cancellative properties of -_, _+_; _*_ and _≤_
  
  neg-cancel-≤ : ∀ {p q} → - p ≤ - q → q ≤ p
  neg-cancel-≤ {p}{q} -p≤-q = begin
    q ≡˘⟨ neg-involutive q ⟩
    - (- q) ≤⟨ neg-antimono-≤ -p≤-q ⟩
    - (- p) ≡⟨ neg-involutive p ⟩
    p ∎
  
  +-cancelˡ-≤ : ∀ {p q} r → r + p ≤ r + q → p ≤ q
  +-cancelˡ-≤ {p}{q} r r+p≤r+q = begin
    p ≡˘⟨ +-identityˡ p ⟩
    0 + p ≡˘⟨ cong (_+ p) $ +-inverseˡ r ⟩
    (- r + r) + p ≡⟨ +-assoc (- r) r p ⟩
    - r + (r + p) ≤⟨ +-monoʳ-≤ (- r) r+p≤r+q ⟩
    - r + (r + q) ≡˘⟨ +-assoc (- r) r q ⟩
    (- r + r) + q ≡⟨ cong (_+ q) $ +-inverseˡ r ⟩
    0 + q ≡⟨ +-identityˡ q ⟩
    q ∎

  +-cancelʳ-≤ : ∀{p q} r → p + r ≤ q + r → p ≤ q
  +-cancelʳ-≤ {p}{q} r rewrite +-comm p r | +-comm q r = +-cancelˡ-≤ r

  *-cancelˡ-≤ : ∀{p q} r → Positive r → r * p ≤ r * q → p ≤ q
  *-cancelˡ-≤ {p}{q} r@(mkℚ (ℤ.+ (suc _)) _ _) r>0 r*p≤r*q = begin
    p ≡˘⟨ *-identityˡ p ⟩
    1 * p ≡˘⟨ cong (_* p) $ *-inverseˡ r ⟩
    (1/ r * r) * p ≡⟨ *-assoc (1/ r) r p ⟩
    1/ r * (r * p) ≤⟨ *-monoˡ-≤-pos (1/ r) _ r*p≤r*q ⟩
    1/ r * (r * q) ≡˘⟨ *-assoc (1/ r) r q ⟩
    (1/ r * r) * q ≡⟨ cong (_* q) $ *-inverseˡ r ⟩
    1 * q ≡⟨ *-identityˡ q ⟩
    q ∎

  *-cancelʳ-≤ : ∀{p q} r → Positive r → p * r ≤ q * r → p ≤ q
  *-cancelʳ-≤ {p}{q} r rewrite *-comm p r | *-comm q r = *-cancelˡ-≤ r

------------------------------------------------------------------------
-- Properties of 1/_
------------------------------------------------------------------------

private
  pos⇒≢0 : ∀ p → Positive p → ℤ.∣ ↥ p ∣ ≢0
  pos⇒≢0 (mkℚ +[1+ n ] d-1 _) p>0 = _

1/-mono-<-pos : (p>0 : Positive p)(p<q : p < q)
  → let q≢0 = pos⇒≢0 q $ positive $ <-trans (positive⁻¹ p>0) p<q
  in ---------------------------------------------------
  (1/ p){pos⇒≢0 p p>0} > (1/ q) {q≢0}
1/-mono-<-pos {mkℚ +[1+ n ] d-1 _}{q} p>0 p<q
  with positive $ <-trans (positive⁻¹ p>0) p<q
1/-mono-<-pos {mkℚ +[1+ n ] d-1 _}{mkℚ +[1+ n' ] d'-1 _} p>0 (*<* p<q) | q>0 =
  *<* $′ begin-strict
  +[1+ d'-1 ] ℤ.* +[1+ n ] ≡⟨ ℤ.*-comm +[1+ d'-1 ] +[1+ n ] ⟩
  +[1+ n ] ℤ.* +[1+ d'-1 ] <⟨ p<q ⟩
  +[1+ n' ] ℤ.* +[1+ d-1 ] ≡⟨ ℤ.*-comm +[1+ n' ] +[1+ d-1 ] ⟩
  +[1+ d-1 ] ℤ.* +[1+ n' ] ∎
  where open ℤ.≤-Reasoning

1/-mono-≤-pos : (p>0 : Positive p)(p≤q : p ≤ q)
  → let q≢0 = pos⇒≢0 q $ positive $ <-≤-trans (positive⁻¹ p>0) p≤q
  in ---------------------------------------------------
  (1/ p){pos⇒≢0 p p>0} ≥ (1/ q) {q≢0}
1/-mono-≤-pos {mkℚ +[1+ n ] d-1 _}{q} p>0 p≤q
  with positive $ <-≤-trans (positive⁻¹ p>0) p≤q
1/-mono-≤-pos {mkℚ +[1+ n ] d-1 _}{mkℚ +[1+ n' ] d'-1 _} p>0 (*≤* p≤q) | q>0 =
  *≤* $′ begin
  +[1+ d'-1 ] ℤ.* +[1+ n ] ≡⟨ ℤ.*-comm +[1+ d'-1 ] +[1+ n ] ⟩
  +[1+ n ] ℤ.* +[1+ d'-1 ] ≤⟨ p≤q ⟩
  +[1+ n' ] ℤ.* +[1+ d-1 ] ≡⟨ ℤ.*-comm +[1+ n' ] +[1+ d-1 ] ⟩
  +[1+ d-1 ] ℤ.* +[1+ n' ] ∎
  where open ℤ.≤-Reasoning

----------------------------------------------------------------------
-- properties of ∣_∣

∣p∣-pos : ∀ {p} → NonZero p → Positive ∣ p ∣
∣p∣-pos {mkℚ +[1+ n ] _ _} p≠0 = _
∣p∣-pos {mkℚ ℤ.-[1+ n ] _ _} p≠0 = _

p≤∣p∣ : ∀ p → p ≤ ∣ p ∣
p≤∣p∣ p@(mkℚ (ℤ.+ n) _ _) = ≤-refl
p≤∣p∣ p@(mkℚ ℤ.-[1+ n ] _ _) = <⇒≤ $ negative<positive _ _

∣p∣≤q : ∀ p q → Positive q → - q ≤ p → p ≤ q → ∣ p ∣ ≤ q
∣p∣≤q p q q>0 -q≤p p≤q with ∣p∣≡p∨∣p∣≡-p p
... | inj₁ ∣p∣≡p rewrite ∣p∣≡p = p≤q
... | inj₂ ∣p∣≡-p rewrite ∣p∣≡-p = begin
  - p ≤⟨ neg-antimono-≤ -q≤p ⟩
  - - q ≡⟨ neg-involutive q ⟩
  q ∎
  where open ≤-Reasoning

∣p∣<q : ∀ p q → Positive q → - q < p → p < q → ∣ p ∣ < q
∣p∣<q p q q>0 -q<p p<q with ∣p∣≡p∨∣p∣≡-p p
... | inj₁ ∣p∣≡p rewrite ∣p∣≡p = p<q
... | inj₂ ∣p∣≡-p rewrite ∣p∣≡-p = begin-strict
  - p <⟨ neg-antimono-< -q<p ⟩
  - - q ≡⟨ neg-involutive q ⟩
  q ∎
  where open ≤-Reasoning
