module Numbers.Nat.Literals where

open import Data.Unit using (tt) public
open import Data.Nat.Literals
open import Agda.Builtin.FromNat using (fromNat) public
instance
  Numberℕ = number
