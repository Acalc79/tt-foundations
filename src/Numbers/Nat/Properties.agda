module Numbers.Nat.Properties where

open import Data.Nat
open import Data.Nat.Properties
open import Data.Nat.Divisibility
open import Data.Nat.DivMod
open import Function
open import Relation.Nullary.Decidable
open import Relation.Binary.PropositionalEquality

private variable
  m n : ℕ 

1<m/n : ∀{m n} → {n≢0 : False (n ≟ 0)} → n < m → n ∣ m  → 1 < (m / n) {n≢0}
1<m/n {m}{n}{n≢0} n<m n∣m = *-cancelʳ-< {n} 1 d $ begin-strict
  1 * n ≡⟨ *-identityˡ n ⟩
  n <⟨ n<m ⟩
  m ≡˘⟨ m/n*n≡m n∣m ⟩
  d * n ∎
  where
  open ≤-Reasoning
  d = (m / n){n≢0}

m*n/m≡n : ∀ m n {≢0} → (m * n / m) {≢0} ≡ n
m*n/m≡n m@(suc _) n = trans (cong (_/ m)(*-comm m n)) (m*n/n≡m n m)
