module Numbers.Nat.Extra where

open import Data.Empty
open import Data.Nat
open import Data.Nat.DivMod
open import Data.Nat.Induction
  using (Acc; acc; <-wellFounded-fast)
open import Data.Nat.Properties
open import Data.Nat.Solver renaming (module +-*-Solver to ℕ-Solver) 
open import Function
open import Induction.WellFounded using () renaming (acc-inverse to unacc)
open import Numbers.Nat.Literals
open import Relation.Binary.PropositionalEquality

open ℕ-Solver
open ≤-Reasoning

private variable
  m n o : ℕ

⌊log2_⌋[_] : (n : ℕ) → Acc _<_ n → .{n≢0 : NonZero n} → ℕ
⌊log2 1 ⌋[ _ ] = 0
⌊log2 n@(suc (suc n')) ⌋[ acc rec ] =
  suc (⌊log2 n / 2 ⌋[ rec (n / 2) (m/n<m n 2 (s≤s z≤n) ≤-refl) ]
       {>-nonZero $ m≥n⇒m/n>0 $ s≤s $ s≤s $ z≤n {n'}})

⌊log2_⌋ : (n : ℕ).{n≢0 : NonZero n} → ℕ
⌊log2 n ⌋ {n≢0} = ⌊log2 n ⌋[ <-wellFounded-fast n ] {n≢0}

------------------------------------------------------------------------
-- Core properties of ⌊log2_⌋[_]

⌊log2⌋′≡ : (m≡n : m ≡ n)
           {rec : Acc _<_ m}{rec' : Acc _<_ n} → 
           .{m≢0 : NonZero m} →
           ⌊log2 m ⌋[ rec ] {m≢0} ≡ ⌊log2 n ⌋[ rec' ] {subst NonZero m≡n m≢0}
⌊log2⌋′≡ {1} refl = refl
⌊log2⌋′≡ {m@(suc (suc m'))} refl {acc rec} {acc rec'} =
  cong suc $ ⌊log2⌋′≡ refl

-- TODO: move to a more appropriate place
nonZero⁻¹ : .(NonZero n) → n > 0
nonZero⁻¹ {suc n} _ = s≤s z≤n

private
  n≢0⇒2n≢0 : .(NonZero n) → NonZero (2 * n)
  0<2ⁿ : ∀ n → 0 < 2 ^ n
  2ⁿ<2ⁿ⁺¹ : ∀ n → 2 ^ n < 2 ^ suc n
  2ⁿ≢0 : ∀ n → NonZero (2 ^ n)

0<2ⁿ n with 2 ^ n in eq
... | zero with () ← m^n≡0⇒m≡0 2 n eq
... | suc _ = s≤s z≤n

2ⁿ<2ⁿ⁺¹ n = m<m+n (2 ^ n) $ subst (0 <_) (sym $ +-identityʳ (2 ^ n)) (0<2ⁿ n)

2ⁿ≢0 n = >-nonZero (0<2ⁿ n)

n≢0⇒2n≢0 {suc _} _ = _

⌊log2-2n⌋′≡suc⌊log2-n⌋′ : ∀ n (rec : Acc _<_ (2 * n))(rec' : Acc _<_ n)
  .{n≢0 : NonZero n}
  → ----------------------------------------------------------------------
  ⌊log2 2 * n ⌋[ rec ] {n≢0⇒2n≢0 n≢0} ≡ suc (⌊log2 n ⌋[ rec' ] {n≢0})
⌊log2-2n⌋′≡suc⌊log2-n⌋′ 1 (acc _) _ = refl
⌊log2-2n⌋′≡suc⌊log2-n⌋′ n@(suc (suc n'))(acc rec)(acc rec') = begin-equality
  ⌊log2 2 * n ⌋[ acc rec ] ≡⟨⟩
  suc ⌊log2 2 * n / 2 ⌋[ _ ]
    ≡⟨ cong suc $ ⌊log2⌋′≡ (trans (cong (_/ 2) (*-comm 2 n)) (m*n/n≡m n 2))
                           {rec' = acc rec'}
                           {m≢0 = 2n/2≢0} ⟩
  suc ⌊log2 n ⌋[ acc rec' ] ∎
  where
  2n/2≢0 = >-nonZero $ m≥n⇒m/n>0 {2 * n}{2} $ *-monoʳ-≤ 2 {1}{n} $ s≤s z≤n

⌊log2-2^n⌋′≡n : ∀ n (rec : Acc _<_ (2 ^ n)) →
  ⌊log2 2 ^ n ⌋[ rec ] {2ⁿ≢0 n} ≡ n
⌊log2-2^n⌋′≡n zero rec = refl
⌊log2-2^n⌋′≡n (suc n) (acc rec) = begin-equality
  ⌊log2 2 ^ suc n ⌋[ acc rec ]
    ≡⟨ ⌊log2-2n⌋′≡suc⌊log2-n⌋′ (2 ^ n)
       (acc rec)(rec (2 ^ n) (2ⁿ<2ⁿ⁺¹ n)){2ⁿ≢0 n} ⟩
  suc ⌊log2 2 ^ n ⌋[ _ ]
    ≡⟨ cong suc $ ⌊log2-2^n⌋′≡n n (rec (2 ^ n) (2ⁿ<2ⁿ⁺¹ n)) ⟩
  suc n ∎

2^⌊log2n⌋′≤n : ∀ n rec .{n≢0 : NonZero n} → 2 ^ ⌊log2 n ⌋[ rec ] {n≢0} ≤ n
2^⌊log2n⌋′≤n 1 _ = ≤-refl
2^⌊log2n⌋′≤n n@(suc (suc n'))(acc rec) =
  let n/2≢0 = >-nonZero $ m≥n⇒m/n>0 $ s≤s $ s≤s $ z≤n {n'}
      acc-n/2 = rec (n / 2) (m/n<m n 2 (s≤s z≤n) ≤-refl)
      prev = ⌊log2 n / 2 ⌋[ acc-n/2 ] {n/2≢0}
  in begin
  2 ^ suc prev ≡⟨⟩
  2 * 2 ^ prev ≤⟨ *-monoʳ-≤ 2 (2^⌊log2n⌋′≤n (n / 2) acc-n/2 {n/2≢0}) ⟩
  2 * (n / 2) ≡⟨ *-comm 2 (n / 2) ⟩
  (n / 2) * 2 ≤⟨ m/n*n≤m n 2 ⟩
  n ∎

n≤2^suc⌊log2n⌋′ : ∀ n rec .{n≢0 : NonZero n} →
  n < 2 ^ suc (⌊log2 n ⌋[ rec ] {n≢0})
n≤2^suc⌊log2n⌋′ 1 _ = s≤s $ s≤s z≤n
n≤2^suc⌊log2n⌋′ n@(suc (suc n'))(acc rec) {n≢0} =
  let n/2≢0 = >-nonZero $ m≥n⇒m/n>0 $ s≤s $ s≤s $ z≤n {n'}
      acc-n/2 = rec (n / 2) (m/n<m n 2 (s≤s z≤n) ≤-refl)
      prev = ⌊log2 n / 2 ⌋[ acc-n/2 ] {n/2≢0}
      prev≤ : suc (n / 2) ≤ 2 ^ suc prev
      prev≤ = n≤2^suc⌊log2n⌋′ (n / 2) acc-n/2 {n/2≢0}
  in begin-strict
  n ≡⟨ m≡m%n+[m/n]*n n 1 ⟩
  n % 2 + (n / 2) * 2 <⟨ +-monoˡ-< ((n / 2) * 2) $ m%n<n n 1 ⟩
  2  + (n / 2) * 2
    ≡⟨ solve 1 (λ n → con 2 :+ n :* con 2 := con 2 :* (con 1 :+ n))
             (λ {_} → refl)(n / 2) ⟩
  2 * (suc (n / 2)) ≤⟨ *-monoʳ-≤ 2 prev≤ ⟩
  2 * 2 ^ suc prev ≡⟨⟩
  2 ^ suc (suc prev) ∎

------------------------------------------------------------------------
-- Core properties of ⌊log2_⌋

2^⌊log2n⌋≤n : ∀ n .{n≢0 : NonZero n} → 2 ^ ⌊log2 n ⌋ {n≢0} ≤ n
2^⌊log2n⌋≤n n {n≢0} = 2^⌊log2n⌋′≤n n _ {n≢0}

n≤2^suc⌊log2n⌋ : ∀ n .{n≢0 : NonZero n} → n < 2 ^ suc (⌊log2 n ⌋ {n≢0})
n≤2^suc⌊log2n⌋ n {n≢0} = n≤2^suc⌊log2n⌋′ n _ {n≢0}

⌊log2-2n⌋≡suc⌊log2-n⌋ : ∀ n .{n≢0 : NonZero n}
  → ----------------------------------------------------------------------
  ⌊log2 2 * n ⌋ {n≢0⇒2n≢0 n≢0} ≡ suc (⌊log2 n ⌋ {n≢0})
⌊log2-2n⌋≡suc⌊log2-n⌋ n = ⌊log2-2n⌋′≡suc⌊log2-n⌋′ n _ _

⌊log2-2^n⌋≡n : ∀ n → ⌊log2 2 ^ n ⌋ {2ⁿ≢0 n} ≡ n
⌊log2-2^n⌋≡n n = ⌊log2-2^n⌋′≡n n _
