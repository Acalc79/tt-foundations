module Numbers.Integer where

open import Data.Integer public
open import Data.Integer.Properties public
open import Data.Integer.GCD public
open import Data.Integer.Solver renaming (module +-*-Solver to ℤ-Solver) public
open import Numbers.Integer.Congruence public
open import Numbers.Integer.DivMod public
open import Numbers.Integer.Extra public
open import Numbers.Integer.Extra.Properties public
open import Numbers.Integer.Properties public
