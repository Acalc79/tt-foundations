module Numbers.Real.Helper where

open import Foundation

open import Numbers.Rational
open import Numbers.Rational.Function
open import Numbers.Rational.Literals
open import Numbers.Real.Base

private variable a b c d : ℚ

tight : a < b → c < d → d - c < b - a → c > a ⊎ d < b
tight {a}{b}{c}{d} a<b c<d d-c<b-a = case <-cmp c a of λ
  { (tri< c<a _ _) → case <-cmp d b of λ
    { (tri< d<b _ _) → inj₂ d<b
    ; (tri≈ _ refl _) → inj₁ $
      +-cancelˡ-< b d-c<b-a ∶ - c < - a
      |> neg-cancel-< ∶ a < c
    ; (tri> _ _ b<d) → ⊥-elim $ <-asym (+-mono-< c<a b<d) $ (begin-strict
      a + d - c - a ≡⟨ cong (_- a) $ +-assoc a d (- c) ⟩
      a + (d - c) - a ≡⟨ cong (_- a) $ +-comm a (d - c) ⟩
      d - c + a - a ≡⟨ +-assoc (d - c) a (- a) ⟩
      d - c + (a - a) ≡⟨ cong (d - c +_) $ +-inverseʳ a ⟩
      d - c + 0 ≡⟨ +-identityʳ (d - c) ⟩
      d - c <⟨ d-c<b-a ⟩
      b - a ≡˘⟨ cong (_- a) $ +-identityʳ b ⟩
      b + 0 - a ≡˘⟨ cong (λ x → b + x - a) $ +-inverseʳ c ⟩
      b + (c - c) - a ≡˘⟨ cong (_- a) $ +-assoc b c (- c) ⟩
      b + c - c - a ≡⟨ cong (λ x → x - c - a) $ +-comm b c ⟩
      c + b - c - a ∎)
      |> +-cancelʳ-< (- a) ∶ a + d - c < c + b - c
      |> +-cancelʳ-< (- c)}
  ; (tri≈ _ refl _) → inj₂ $ +-cancelʳ-< (- a) d-c<b-a
  ; (tri> _ _ c<a) → inj₁ c<a }
  where open ≤-Reasoning

mon-inc-map :
  (inv : Inverse (setoid ℚ) (setoid ℚ)) →
  Inverse.f inv Preserves _<_ ⟶ _<_ →
  --------------------------------------------------
  ℝ → ℝ
mon-inc-map inv mon-f x = record
  { L = f⁻¹ ⊢ L
  ; R = f⁻¹ ⊢ R
  ; L-inhabit = Prod.map f yesˡ L-inhabit
  ; R-inhabit = Prod.map f yesʳ R-inhabit
  ; L-no-max = λ {a} a∈fL → case L-no-max a∈fL of λ
                 { (b , b∈L , f⁻¹a<b) → f b , yesˡ b∈L ,
                   subst (_< f b) (inverseˡ a) (mon-f f⁻¹a<b)}
  ; R-no-min = λ {a} a∈fR → case R-no-min a∈fR of λ
                 { (b , b∈R , b<f⁻¹a) → f b , yesʳ b∈R ,
                   subst (f b <_) (inverseˡ a) (mon-f b<f⁻¹a)}
  ; complete = complete ∘ ⁻¹-mono-strict-inc inv mon-f
  ; L<R = λ {a}{b} f⁻¹a∈L f⁻¹b∈R → begin-strict
    a ≡˘⟨ inverseˡ a ⟩
    f (f⁻¹ a) <⟨ mon-f (L<R f⁻¹a∈L f⁻¹b∈R) ⟩
    f (f⁻¹ b) ≡⟨ inverseˡ b ⟩
    b ∎
  }
  where
  open Inverse inv
  open ℝ x
  open ≤-Reasoning
  yesˡ : ∀{a} → a ∈ L → f a ∈ f⁻¹ ⊢ L
  yesˡ {a} = subst (_∈ L) (sym $ inverseʳ a)
  yesʳ : ∀{a} → a ∈ R → f a ∈ f⁻¹ ⊢ R
  yesʳ {a} = subst (_∈ R) (sym $ inverseʳ a)

known-lim-behav : (f : ℚ → ℚ) → Set
known-lim-behav f = {!!} ⊎ {!!}

ivp-map : (f : ℚ → ℚ) → f Preserves _<_ ⟶ _<_ → ivp f → ℝ → ℝ
ivp-map f f-mono f-ivp x = record
  { L = L'
  ; R = R'
  ; L-inhabit = qₗ , qₗ∈L'
  ; R-inhabit = qᵣ , qᵣ∈R'
  ; L-no-max = {!!}
  ; R-no-min = {!!}
  ; complete = λ {p}{q} p<q → case f qₗ ≤? p of λ
    { (no fqₗ≰p) → {!!}
    ; (yes fqₗ≤p) → {!!}}
  ; L<R = λ { {p}{q}(p' , p'∈L , fp'>p)(q' , q'∈R , fq'<q) →
    decidable-stable (p <? q) λ p≮q →
    <-asym (f-mono $ L<R p'∈L q'∈R) $ begin-strict
      f q' <⟨ fq'<q ⟩
      q ≤⟨ ≮⇒≥ p≮q ⟩
      p <⟨ fp'>p ⟩
      f p' ∎}
  }
  where
  open ℝ x
  open ≤-Reasoning
  L' R' : ℙ ℚ
  L' = ｛ p ∣ ∃[ q ∈ L ] (f q > p) ｝
  R' = ｛ p ∣ ∃[ q ∈ R ] (f q < p) ｝
  qₗ = f (proj₁ L-inhabit)
  qₗ∈L' : qₗ ∈ L'
  qₗ∈L' = case (L-no-max $ proj₂ L-inhabit) of λ
    { (q , q∈L , p<q) → q , q∈L , f-mono p<q}
  qᵣ = f (proj₁ R-inhabit)
  qᵣ∈R' : qᵣ ∈ R'
  qᵣ∈R' = case (R-no-min $ proj₂ R-inhabit) of λ
    { (q , q∈R , p<q) → q , q∈R , f-mono p<q}

cont⁻¹ : (f : ℚ → ℚ) → f Preserves _<_ ⟶ _<_ → cont f → ℝ → ℝ
cont⁻¹ = {!!}
