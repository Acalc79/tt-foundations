module Numbers.Real.Base where

open import Foundation

open import Numbers.Integer using (ℤ)
open import Numbers.Rational as Q using (ℚ; ℤ→ℚ; ℕ→ℚ; avg)
open import Numbers.Rational.Literals

private variable
  p q : ℚ

record DedekindCut : Set₁ where
  field
    L R : ℙ ℚ
    L-inhabit : Inhabited L
    R-inhabit : Inhabited R
    L-no-max : p ∈ L → ∃[ q ] q ∈ L × p Q.< q
    R-no-min : p ∈ R → ∃[ q ] q ∈ R × q Q.< p
    complete : p Q.< q → p ∈ L ⊎ q ∈ R
    L<R : p ∈ L → q ∈ R → p Q.< q

ℝ = DedekindCut
module ℝ = DedekindCut

ℚ→ℝ : ℚ → ℝ
ℝ.R (ℚ→ℝ q) = q Q.<_
ℝ.L (ℚ→ℝ q) = Q._< q
ℝ.L-inhabit (ℚ→ℝ q) = q Q.- 1 , (begin-strict
  q Q.- 1 ≡⟨⟩
  q Q.+ (Q.- 1) <⟨ Q.+-monoʳ-< q {Q.- 1} $ Q.negative⁻¹ _ ⟩
  q Q.+ 0 ≡⟨ Q.+-identityʳ q ⟩
  q ∎)
  where open Q.≤-Reasoning
ℝ.R-inhabit (ℚ→ℝ q) = q Q.+ 1 , Q.p<p+q {q}{1} _
ℝ.L-no-max (ℚ→ℝ q) {a} a<q = avg q a , Q.avg-<ˡ a<q , Q.<-avgʳ a<q
ℝ.R-no-min (ℚ→ℝ q) {b} q<b = avg q b , Q.<-avgˡ q<b , Q.avg-<ʳ q<b
ℝ.complete (ℚ→ℝ q) {a}{b} a<b with Q.<-cmp a q
... | tri< a<q _ _ = inj₁ a<q
... | tri≈ _ refl _ = inj₂ a<b
... | tri> _ _ q<a = inj₂ $ Q.<-trans q<a a<b
ℝ.L<R (ℚ→ℝ q) = Q.<-trans

ℤ→ℝ : ℤ → ℝ
ℤ→ℝ = ℚ→ℝ ∘ ℤ→ℚ

ℕ→ℝ : ℕ → ℝ
ℕ→ℝ = ℚ→ℝ ∘ ℕ→ℚ

-- _+_ : (x y : ℝ) → ℝ
-- x + y = record
--   { L = L
--   ; R = R
--   ; L-inhabit = Lqx Q.+ Lqy , Lqx , Lqy , Lqx∈Lx , Lqy∈Ly , refl
--   ; R-inhabit = Rqx Q.+ Rqy , Rqx , Rqy , Rqx∈Rx , Rqy∈Ry , refl
--   ; L-no-max = L-no-max
--   ; R-no-min = R-no-min
--   ; complete = complete
--   ; L<R = L<R }
--   where
--     L R : ℙ ℚ
--     L c = ∃₂ λ a b → a ∈ ℝ.L x × b ∈ ℝ.L y × a Q.+ b ≡ c
--     R c = ∃₂ λ a b → a ∈ ℝ.R x × b ∈ ℝ.R y × a Q.+ b ≡ c
--     Lqx Lqy Rqx Rqy : ℚ
--     Lqx = proj₁ (ℝ.L-inhabit x)
--     Lqy = proj₁ (ℝ.L-inhabit y)
--     Rqx = proj₁ (ℝ.R-inhabit x)
--     Rqy = proj₁ (ℝ.R-inhabit y)
--     Lqx∈Lx = proj₂ (ℝ.L-inhabit x)
--     Lqy∈Ly = proj₂ (ℝ.L-inhabit y)
--     Rqx∈Rx = proj₂ (ℝ.R-inhabit x)
--     Rqy∈Ry = proj₂ (ℝ.R-inhabit y)
--     L-no-max : a ∈ L → ∃[ b ] b ∈ L × a Q.< b
--     L-no-max (a , b , a∈Rx , b∈Ry , refl) =
--       case ℝ.L-no-max x a∈Rx ,′ ℝ.L-no-max y b∈Ry of λ
--       { ((a' , a'∈Rx , a'<a) , b' , b'∈Ry , b'<b) →
--         a' Q.+ b' ,
--         (a' , b' , a'∈Rx , b'∈Ry , refl) ,
--         Q.+-mono-< a'<a b'<b }
--     R-no-min : b ∈ R → ∃[ a ] a ∈ R × a Q.< b
--     R-no-min (a , b , a∈Rx , b∈Ry , refl) =
--       case ℝ.R-no-min x a∈Rx ,′ ℝ.R-no-min y b∈Ry of λ
--       { ((a' , a'∈Rx , a'<a) , b' , b'∈Ry , b'<b) →
--         a' Q.+ b' ,
--         (a' , b' , a'∈Rx , b'∈Ry , refl) ,
--         Q.+-mono-< a'<a b'<b }
--     L<R : a ∈ L → b ∈ R → a Q.< b
--     L<R (a₀ , a₁ , a₀∈Lx , a₁∈Ly , refl) (b₀ , b₁ , b₀∈Rx , b₁∈Ry , refl) =
--       Q.+-mono-< (ℝ.L<R x a₀∈Lx b₀∈Rx) (ℝ.L<R y a₁∈Ly b₁∈Ry)
--     complete : a Q.< b → a ∈ L ⊎ b ∈ R
--     complete = {!!}

