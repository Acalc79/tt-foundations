module Numbers.Real.Arithmetic where

open import Foundation

open import Data.Nat as ℕ using (ℕ; suc)
open import Numbers.Integer as ℤ
  using (ℤ; ℕ→ℤ; module ℤ-Solver) renaming (+_ to z+_)
open import Numbers.Integer.Literals
open import Numbers.Rational
open import Numbers.Rational.Literals
open import Numbers.Real.Base
open import Numbers.Real.Helper
open import Relation.Nullary.Decidable

-- arbitrarily chose sqrt of negative numbers to be 0
sqrt : ℚ → ℝ
sqrt q = record
  { L = L
  ; R = R
  ; L-inhabit = -1 , inj₁ (*<* ℤ.-<+)
  ; R-inhabit = ∣ q ∣ + 1 ,
    ≤-<-trans (nonNegative⁻¹ {∣ q ∣} $ ∣-∣-nonNeg q) (p<p+q {∣ q ∣}{1} _) ,
    (let open ≤-Reasoning in begin-strict
      q ≤⟨ p≤∣p∣ q ⟩
      ∣ q ∣ <⟨ p<p+q {∣ q ∣}{1} _ ⟩
      ∣ q ∣ + 1 ≤⟨ ^-≤ 1 $ p≤q+p {∣ q ∣} $ ∣-∣-nonNeg q ⟩
      (∣ q ∣ + 1)² ∎) 
  ; L-no-max = 
    λ {a} → case 0 ≤? a of λ
      { (no 0≰a) → λ _ → avg a 0 , inj₁ (avg-<ʳ $ ≰⇒> 0≰a) , <-avgˡ (≰⇒> 0≰a)
      ; (yes 0≤a) → aux-L-no-max 0≤a}
    -- { {a}(inj₁ a<0) → avg a 0 , inj₁ (avg-<ʳ a<0) , <-avgˡ a<0
    --   ; {a} (inj₂ a²<q) →
    --     let an = ↥ a
    --         ad = ↧ a
    --         N = 2 ℤ.* an ℤ.+ 2 -- from https://www.math.brown.edu/reschwar/INF/handout3.pdf
    --         b = {!!} -- (N ℤ.* an ℤ.+ 1) / (N ℤ.* ad)
    --     in
    --     b ,
    --     {!!} ,
    --     {!!}}
  ; R-no-min = λ { {b}(b>0 , b²>q) → {!!} , {!!} , {!!}}
  ; complete = λ {a}{b} a<b → case a <? 0 of λ
    { (yes a<0) → inj₁ (inj₁ a<0)
    ; (no a≮0) → case a * a <? q of λ
      { (yes a²<q) → inj₁ (inj₂ a²<q)
      ; (no a²≮q) → inj₂ $
        ≤-<-trans (≮⇒≥ a≮0) a<b ,
        ≤-<-trans (≮⇒≥ a²≮q) (^-mono-<-nonNeg (nonNegative $ ≮⇒≥ a≮0) 1 a<b) }}
  ; L<R = λ { {a} {b} (inj₁ a<0) (b>0 , b²>q) → <-trans a<0 b>0
            ; {a} {b} (inj₂ a²<q) (b>0 , b²>q) →
              ^-cancel-<-nonNeg (positive⇒nonNegative {b} $ positive b>0) 1 $
              let open ≤-Reasoning in begin-strict
                a * a <⟨ a²<q ⟩
                q <⟨ b²>q ⟩
                b * b ∎}
  }
  where
  L R : ℙ ℚ
  L x = x < 0 ⊎ x ^ 2 < q
  R x = x > 0 × x ^ 2 > q
  -- from https://www.math.brown.edu/reschwar/INF/handout3.pdf
  aux-L-no-max : ∀ {p} → 0 ≤ p → p ∈ L → ∃[ r ] r ∈ L × p < r
  aux-L-no-max 0≤p (inj₁ p<0) = ⊥-elim $ <⇒≱ p<0 0≤p
  aux-L-no-max {p@(mkℚ pn _ _)} 0≤p (inj₂ p²<q) with 0≤p⇒0≤↥p 0≤p
  aux-L-no-max {p@(mkℚ (z+ pn) _ _)} 0≤p (inj₂ p²<q) | _ =
    let n = 2 * p + 1
        d = q - p ² in
    goal ∃[ r ] r ∈ L × p < r
    from (λ { (N , r∈L , p<r) → let r = p + 1/ (N +1') in r , r∈L , p<r })
    RTP: ∃[ N ] (let r = p + 1/ (N +1') in r ∈ L × p < r)
    from (λ { (N , r²<q) → N , inj₂ r²<q , p<p+q {p} {1/ (N +1')} _})
    RTP: ∃[ N ] (p + 1/ (N +1')) ² < q
    from (λ {(N , Np²+2p+1/N<qN) →
      N , *-cancelʳ-< (N +1') _ (let N' = N +1' in begin
      (p + 1/ N') ² * N'
        ≡⟨ solve 3 (λ p 1/N N →
             (p :+ 1/N) :^ 2 :* N :=
             N :* p :^ 2 :+ 2 :× p :* (N :* 1/N) :+ 1/N :* (N :* 1/N))
           (λ {_}{_}{_} → refl) {!p!} (1/ N') N' ⟩
      N' * p ² + 2 * p * (N' * 1/ N') + 1/ N' * (N' * 1/ N')
        ≡⟨ {!!} ⟩
      N' * p ² + 2 * p * 1 + 1/ N' * 1
        ≡⟨ {!!} ⟩
      N' * p ² + 2 * p + 1/ N' <⟨ Np²+2p+1/N<qN ⟩
      q * N' ∎)})
    RTP: ∃[ N ] N +1' * p ² + 2 * p + 1/ (N +1') < q * (N +1')
    from (λ { (N , Nd+2p+1/N<0) →
      N , +-cancelˡ-< (- (q * (N +1'))) (begin
      - (q * (N +1')) + (N +1' * p ² + 2 * p + 1/ (N +1'))
        ≡⟨ solve 5 (λ N q p² 2p 1/N →
             :- (q :* N) :+ (N :* p² :+ 2p :+ 1/N) := N :* :- (q :- p²) :+ 2p :+ 1/N)
           (λ {_}{_}{_}{_}{_} → refl)(N +1') q (p ²)(2 * p)(1/ (N +1')) ⟩
      N +1' * - d + 2 * p + 1/ (N +1') <⟨ Nd+2p+1/N<0 ⟩
      0 ≡˘⟨ +-inverseˡ (q * (N +1')) ⟩
      - (q * (N +1')) + q * (N +1') ∎)})
    RTP: ∃[ N ] N +1' * - d + 2 * p + 1/ (N +1') < 0
    from (λ { (N , Nd+n<0) → N , (begin
      N +1' * - d + 2 * p + 1/ (N +1')
        ≤⟨ +-monoʳ-≤ ((N +1') * - d + 2 * p) $
           1/-mono-≤-pos {1}{N +1'} _ $
           *≤* $ ℤ.+≤+ $ ℕ.s≤s ℕ.z≤n ⟩
      N +1' * - d + 2 * p + 1 ≡⟨ +-assoc (N +1' * - d)(2 * p) 1 ⟩
      N +1' * - d + n <⟨ Nd+n<0 ⟩
      0 ∎)})
    RTP: ∃[ N ] N +1' * - d + n < 0
    from (λ { (N , Nd>n) → N , (
         neg-mono-< Nd>n                           ∶ - (N +1' * d) < - n
      |> subst (_< - n) (neg-distribʳ-* (N +1') d)  ∶ N +1' * - d < - n
      |> +-monoˡ-< n                                ∶ N +1' * - d + n < - n + n
      |> subst ((N +1') * - d + n <_)(+-inverseˡ n) ∶ N +1' * - d + n < 0)})
    RTP: ∃[ N ] N +1' * d > n
    from Prod.map₂ (λ {N} N>n/d →
      let 1/d = (1/ d) {d≢0}
          1/d>0 = pos⇒1/pos d $ positive $ p<q⇒0<q-p p²<q
      in *-cancelʳ-< 1/d 1/d>0 $ begin
      n * 1/d <⟨ N>n/d ⟩
      N +1' ≡˘⟨ *-identityʳ (N +1') ⟩
      N +1' * 1 ≡˘⟨ cong (N +1' *_) $ *-inverseʳ d {d≢0} ⟩
      N +1' * (d * 1/d) ≡˘⟨ *-assoc (N +1') d 1/d ⟩
      N +1' * d * 1/d ∎)
    RTP: ∃[ N ] N +1' > ((n) ÷ d) {d≢0}
    from let k = (n ÷ d) {d≢0}
             N = ℤ.∣ ⌈ k ⌉ ∣ in
         N , (begin
         k ≤⟨ p≤⌈p⌉ ⟩
         ℤ→ℚ ⌈ k ⌉ ≤⟨ ℤ→ℚ-mono-≤ $ ℤ.i≤∣i∣ ⌈ k ⌉ ⟩
         ℤ→ℚ (ℤ.+ ℤ.∣ ⌈ k ⌉ ∣) ≡⟨⟩
         ℤ→ℚ (ℕ→ℤ N) <⟨ ℤ→ℚ-mono-< $ ℤ.suc[i]≤j⇒i<j ℤ.≤-refl ⟩
         ℤ→ℚ (ℤ.suc (ℕ→ℤ N)) ≡⟨⟩
         ℕ→ℚ (ℕ.suc N) ∎)
    where
    open ℚ-Solver
    open ≤-Reasoning hiding (begin_) renaming (begin-strict_ to begin_)
    d≢0 = NonZero-≢0 (q - p ²) $ >-nonZero $ p<q⇒0<q-p p²<q
    _+1' = ℕ→ℚ ∘ suc
