{-# OPTIONS --without-K --safe #-}
module Numbers.Real.Properties where

import Data.Nat as ℕ
open import Foundation
import Numbers.Integer as ℤ
open import Numbers.Integer.Literals
open import Numbers.Rational
open import Numbers.Real.Base
open import Relation.Nullary.Decidable

private
  variable
    a b c : ℚ

module _ (x : ℝ) where
  open ℝ x

  L-closed-down : a ∈ L → b < a → b ∈ L
  L-closed-down {a}{b} a∈L b<a =
    complete b<a ∶ b ∈ L ⊎ a ∈ R
    |> fromInj₁ λ a∈R → ⊥-elim $ <-irrefl refl $ L<R a∈L a∈R

  R-closed-up : a ∈ R → a < b → b ∈ R
  R-closed-up {a} {b} a∈R a<b =
    complete a<b ∶ a ∈ L ⊎ b ∈ R
    |> fromInj₂ λ a∈L → ⊥-elim $ <-irrefl refl $ L<R a∈L a∈R

  private module _ where
    open ≤-Reasoning
    open ℚ-Solver

    1/3 = 1 / 3
    2/3 = 2 / 3
    1/3<2/3 : 1/3 < 2/3
    1/3<2/3 = from-yes (1/3 <? 2/3)

    p₀ q₀ : ℚ
    p₀ = proj₁ L-inhabit
    q₀ = proj₁ R-inhabit
    -- with abstraction is really slow here
    -- thus this complex type is needed
    p∈L,q∈R,p-q≡ : ∀ n → ∃₂ λ p q →
      p ∈ L × q ∈ R × p - q ≡ (p₀ - q₀) * 2/3 ^ n
    p∈L,q∈R,p-q≡ 0 = p₀ , q₀ ,
      proj₂ L-inhabit ,
      proj₂ R-inhabit ,
      sym (*-identityʳ (p₀ - q₀))
    p∈L,q∈R,p-q≡ (ℕ.suc n) = case p∈L,q∈R,p-q≡ n of λ
      { (p , q , p∈L , q∈R , p-q≡) →
        let cl = wavg p 2/3 q
            cr = wavg p 1/3 q
            p<q = L<R p∈L q∈R
            p-q≡-step : (p - q) * 2/3 ≡ (p₀ - q₀) * 2/3 ^ ℕ.suc n
            p-q≡-step = begin-equality
              (p - q) * 2/3               ≡⟨ cong (_* 2/3) p-q≡ ⟩
              (p₀ - q₀) * 2/3 ^ n * 2/3   ≡⟨ *-assoc (p₀ - q₀) (2/3 ^ n) 2/3 ⟩
              (p₀ - q₀) * (2/3 ^ n * 2/3)
                ≡⟨ cong ((p₀ - q₀) *_) $ *-comm (2/3 ^ n) 2/3 ⟩
              (p₀ - q₀) * (2/3 * 2/3 ^ n) ≡˘⟨ cong ((p₀ - q₀) *_) $ ^suc 2/3 n ⟩
              (p₀ - q₀) * 2/3 ^ ℕ.suc n ∎
        in case complete (wavg<wavg p<q 1/3<2/3) of λ
        { (inj₁ cl∈L) → cl , q , cl∈L , q∈R , (begin-equality
          cl - q ≡⟨⟩
          p * 2/3 + q * 1/3 - q
            ≡⟨ solve 2 (λ p q → p :* con 2/3 :+ q :* con 1/3 :- q :=
                                (p :- q) :* con 2/3)
                     (λ {_}{_} → refl) p q ⟩
          (p - q) * 2/3               ≡⟨ p-q≡-step ⟩
          (p₀ - q₀) * (2/3) ^ ℕ.suc n ∎)
        ; (inj₂ cr∈R) → p , cr , p∈L , cr∈R , (begin-equality
          p - cr ≡⟨⟩
          p - (p * 1/3 + q * 2/3)
            ≡⟨ solve 2 (λ p q → p :- (p :* con 1/3 :+ q :* con 2/3) :=
                                (p :- q) :* con 2/3)
                     (λ {_}{_} → refl) p q ⟩
          (p - q) * 2/3               ≡⟨ p-q≡-step ⟩
          (p₀ - q₀) * (2/3) ^ ℕ.suc n ∎)}}

  L-R-close : ∀ δ → ∃₂ λ p q → p ∈ L × q ∈ R × q - p < δ
  L-R-close δ = {!!}

-- (q₀ - p₀) * 2/3 ^ n < δ

-- 2/3 ^ n < δ / (q₀ - p₀)

-- n > log 2/3
