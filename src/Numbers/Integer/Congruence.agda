module Numbers.Integer.Congruence where

open import Algebra
open import Data.Fin as Fin using (Fin)
import Data.Fin.Properties as Fin
open import Data.Integer
open import Data.Integer.Divisibility.Signed
open import Data.Integer.Properties
open import Data.Integer.Solver
open import Foundation
open import Numbers.Integer.DivMod
open import Numbers.Integer.Literals
open import Numbers.Integer.Properties
import Numbers.Nat as ℕ
open import Relation.Binary
import Relation.Binary.Reasoning.Setoid as SetoidReasoning
open +-*-Solver

private variable
  i j k m : ℤ

------------------------------------------------------------------------
-- Type

infix 4 _≡_[mod_]
record _≡_[mod_] (i j m : ℤ) : Set where
  constructor congruent
  field to-∣ : m ∣ i - j
open _≡_[mod_] using (to-∣) public

------------------------------------------------------------------------
-- decidability

_≟_[mod_] : ∀ i j m → Dec (i ≡ j [mod m ])
i ≟ j [mod m ] = DEC.map′ congruent to-∣ (m ∣? i - j)

------------------------------------------------------------------------
-- _≡_[mod_] is an equivalence relation
  
module _ {m} where
  infix 4  _≡ₘ_
  private _≡ₘ_ = _≡_[mod m ]

  ≡ₘ-refl : Reflexive _≡ₘ_
  ≡ₘ-refl {i} = congruent $ subst (m ∣_) (sym $ +-inverseʳ i) $ m ∣0
  
  ≡ₘ-reflexive : _≡_ ⇒ _≡ₘ_
  ≡ₘ-reflexive refl = ≡ₘ-refl

  ≡ₘ-trans : Transitive _≡ₘ_
  ≡ₘ-trans {i}{j}{k}(congruent m∣i-j)(congruent m∣j-k) = congruent $
    subst (m ∣_)
      (solve 3 (λ i j k → i :- j :+ (j :- k) := i :- k)
             (λ{_}{_}{_} → refl) i j k) $
    ∣m∣n⇒∣m+n m∣i-j m∣j-k

  ≡ₘ-sym : Symmetric _≡ₘ_
  ≡ₘ-sym {i}{j}(congruent m∣i-j) = congruent $
    subst (m ∣_)
      (solve 2 (λ i j → :- (i :- j) := j :- i)(λ{_}{_} → refl) i j) $
    ∣m⇒∣-m m∣i-j

  ≡ₘ-isEquivalence : IsEquivalence _≡ₘ_
  ≡ₘ-isEquivalence = record
    { refl = ≡ₘ-refl
    ; sym = ≡ₘ-sym
    ; trans = ≡ₘ-trans}

  ≡ₘ-setoid : Setoid _ _
  ≡ₘ-setoid = record { isEquivalence = ≡ₘ-isEquivalence }

  ≡ₘ-isDecEquivalence : IsDecEquivalence _≡ₘ_
  ≡ₘ-isDecEquivalence = record
    { isEquivalence = ≡ₘ-isEquivalence
    ; _≟_ = _≟_[mod m ] }

  ≡ₘ-decSetoid : DecSetoid _ _
  ≡ₘ-decSetoid = record { isDecEquivalence = ≡ₘ-isDecEquivalence }

------------------------------------------------------------------------
-- Congruence reasoning

module ≡ₘ-Reasoning m where
  private
    module Base = SetoidReasoning (≡ₘ-setoid {m})

  open Base public
    hiding (step-≈; step-≈˘)

  infixr 2 step-≡ₘ step-≡ₘ˘

  step-≡ₘ = Base.step-≈
  syntax step-≡ₘ i j≡ₘk i≡ₘj = i ≡ₘ⟨ i≡ₘj ⟩ j≡ₘk

  step-≡ₘ˘ = Base.step-≈˘
  syntax step-≡ₘ˘ i j≡ₘk j≡ₘi = i ≡ₘ˘⟨ j≡ₘi ⟩ j≡ₘk

------------------------------------------------------------------------
-- Other properties of _≡_[mod_]

≡₁ : i ≡ j [mod 1 ]
≡₁ {i}{j} = congruent (1∣ i - j)

≡ₘ⇒≡ : NonNegative i → NonNegative j → i < m → j < m
  → --------------------------------------------------
  i ≡ j [mod m ] → i ≡ j
≡ₘ⇒≡ {i}{j}{m} i≥0 j≥0 i<m j<m i≡ₘj = case i≡ₘj of λ
  { (congruent (divides (+ 0) i-j≡qm)) →
    m-n≡0⇒m≡n i j $ trans i-j≡qm (*-zeroˡ m)
  ; (congruent (divides +[1+ n ] i-j≡qm)) → ⊥-elim $ <-irrefl refl $
    <-≤-trans i-j<m $ begin
    m ≡˘⟨ +-identityʳ m ⟩
    m + 0 ≡˘⟨ cong (_+_ m) $ *-zeroʳ (+ n) ⟩
    m + + n * 0 ≤⟨ +-monoʳ-≤ m $ *-monoˡ-≤-nonNeg n $ <⇒≤ 0<m ⟩
    m + + n * m ≡˘⟨ cong (_+ + n * m) $ *-identityˡ m ⟩
    1 * m + + n * m ≡˘⟨ *-distribʳ-+ m 1 (+ n) ⟩
    (1 + + n) * m ≡˘⟨ i-j≡qm ⟩
    i - j ∎
  ; (congruent (divides -[1+ n ] i-j≡qm)) → ⊥-elim $ <-irrefl refl $
    <-≤-trans -m<i-j $ begin
    i - j ≡⟨ i-j≡qm ⟩
    -[1+ n ] * m ≡⟨ cong (_* m) $ neg-suc n ⟩
    (-1 - + n) * m ≡⟨ *-distribʳ-+ m -1 (- + n) ⟩
    -1 * m + (- + n) * m
      ≡⟨ cong₂ _+_ (-1*n≡-n m) (sym $ neg-distribˡ-* (+ n) m) ⟩
    - m - + n * m
      ≤⟨ +-monoʳ-≤ (- m) $ neg-mono-≤ $ *-monoˡ-≤-nonNeg n (<⇒≤ 0<m) ⟩
    - m - + n * 0 ≡⟨ cong (_-_ (- m)) $ *-zeroʳ (+ n) ⟩
    - m - 0 ≡⟨ +-identityʳ (- m) ⟩
    - m ∎
  }
  where
  open ≤-Reasoning
  i-j<m : i - j < m
  i-j<m = begin-strict
    i - j ≤⟨ +-monoʳ-≤ i $ neg-mono-≤ $ nonNegative⁻¹ j≥0 ⟩
    i - 0 ≡⟨ +-identityʳ i ⟩
    i <⟨ i<m ⟩
    m ∎
  -m<i-j : - m < i - j
  -m<i-j = begin-strict
    - m <⟨ neg-mono-< j<m ⟩
    - j ≡˘⟨ +-identityˡ (- j) ⟩
    0 - j ≤⟨ +-monoˡ-≤ (- j) $ nonNegative⁻¹ i≥0 ⟩
    i - j ∎
  0<m : 0 < m
  0<m = ≤-<-trans (nonNegative⁻¹ i≥0) i<m

≡ₘmod : ∀ i → {m≢0 : False (∣ m ∣ ℕ.≟ 0)}
  → ----------------------------------------
  i ≡ + (i mod m) {m≢0} [mod m ]
≡ₘmod {m} i {m≢0} = congruent $
  subst (m ∣_) (begin
    q * m ≡⟨ solve 2 (λ qm r → qm := r :+ qm :- r)
                   (λ{_}{_} → refl) (q * m) r ⟩
    r + q * m - r ≡˘⟨ cong (_- r) $ a≡a%n+[a/n]*n i m {m≢0} ⟩
    i - r ∎) $
  ∣n⇒∣m*n q ∣-refl
  where
  open ≡-Reasoning
  r = + (i mod m) {m≢0}
  q = (i div m) {m≢0}

≡ₘmodFin : ∀ {m} i → {m≢0 : False (∣ m ∣ ℕ.≟ 0)}
  → -----------------------------------------------
  i ≡ + (Fin.toℕ ((i modFin ∣ m ∣) {m≢0})) [mod m ]
≡ₘmodFin {m} i {m≢0} =
  subst (λ r → i ≡ + r [mod m ])
        (sym $ Fin.toℕ-fromℕ< (n%ℕd<d i ∣ m ∣ {m≢0})) $
  ≡ₘmod {m} i {m≢0}

------------------------------------------------------------------------
-- Congruence properties of _≡_[mod_]
module _ {m} where
  infix 4 _≡ₘ_
  private _≡ₘ_ = _≡_[mod m ]

  +-cong-≡ₘ : Congruent₂ _≡ₘ_ _+_
  +-cong-≡ₘ {i}{j}{k}{l}(congruent m∣i-j)(congruent m∣k-l) = congruent $
    subst (m ∣_)
      (solve 4 (λ i j k l → i :- j :+ (k :- l) := i :+ k :- (j :+ l))
             (λ{_}{_}{_}{_} → refl) i j k l) $
    ∣m∣n⇒∣m+n m∣i-j m∣k-l

  +-congʳ-≡ₘ : ∀ i → Congruent₁ _≡ₘ_ (_+_ i)
  +-congʳ-≡ₘ i = +-cong-≡ₘ (≡ₘ-refl {m}{i})

  +-congˡ-≡ₘ : ∀ i → Congruent₁ _≡ₘ_ (_+ i)
  +-congˡ-≡ₘ i j≡ₘk = +-cong-≡ₘ j≡ₘk (≡ₘ-refl {m}{i})

  neg-cong-≡ₘ : Congruent₁ _≡ₘ_ (-_)
  neg-cong-≡ₘ {i}{j}(congruent m∣i-j) = congruent $
    subst (m ∣_)
      (solve 2 (λ i j → :- (i :- j) := :- i :- (:- j))
             (λ{_}{_} → refl) i j) $
    ∣m⇒∣-m m∣i-j

  -‿cong-≡ₘ : Congruent₂ _≡ₘ_ _-_
  -‿cong-≡ₘ i≡ₘj k≡ₘl = +-cong-≡ₘ i≡ₘj (neg-cong-≡ₘ k≡ₘl)

  -‿congʳ-≡ₘ : ∀ i → Congruent₁ _≡ₘ_ (_-_ i)
  -‿congʳ-≡ₘ i = -‿cong-≡ₘ (≡ₘ-refl {m}{i})

  -‿congˡ-≡ₘ : ∀ i → Congruent₁ _≡ₘ_ (_- i)
  -‿congˡ-≡ₘ i j≡ₘk = -‿cong-≡ₘ j≡ₘk (≡ₘ-refl {m}{i})

  *-cong-≡ₘ : Congruent₂ _≡ₘ_ _*_
  *-cong-≡ₘ {i}{j}{k}{l}(congruent m∣i-j)(congruent m∣k-l) = congruent $
    subst (m ∣_)
      (solve 4 (λ i j k l → (i :- j) :* k :+ j :* (k :- l) :=
                            i :* k :- j :* l)
             (λ{_}{_}{_}{_} → refl) i j k l) $
    ∣m∣n⇒∣m+n (∣m⇒∣m*n k m∣i-j)(∣n⇒∣m*n j m∣k-l)
    ∶ m ∣ (i - j) * k + j * (k - l)

  *-congʳ-≡ₘ : ∀ i → Congruent₁ _≡ₘ_ (i *_)
  *-congʳ-≡ₘ i = *-cong-≡ₘ (≡ₘ-refl {m}{i})

  *-congˡ-≡ₘ : ∀ i → Congruent₁ _≡ₘ_ (_* i)
  *-congˡ-≡ₘ i j≡ₘk = *-cong-≡ₘ j≡ₘk (≡ₘ-refl {m}{i})

  +-cancelʳ : ∀{k i j} → i + k ≡ₘ j + k → i ≡ₘ j
  +-cancelʳ {k}{i}{j} i+k≡ₘj+k = begin
    i ≡⟨ solve 2 (λ i k → i := i :+ k :- k)(λ{_}{_} → refl) i k ⟩
    i + k - k ≡ₘ⟨ -‿congˡ-≡ₘ k i+k≡ₘj+k ⟩
    j + k - k ≡˘⟨ solve 2 (λ i k → i := i :+ k :- k)(λ{_}{_} → refl) j k ⟩
    j ∎
    where open ≡ₘ-Reasoning m

  +-cancelˡ : k + i ≡ₘ k + j → i ≡ₘ j
  +-cancelˡ {k}{i}{j} k+i≡ₘk+j = +-cancelʳ $ begin
    i + k ≡⟨ +-comm i k ⟩
    k + i ≡ₘ⟨ k+i≡ₘk+j ⟩
    k + j ≡⟨ +-comm k j ⟩
    j + k ∎
    where open ≡ₘ-Reasoning m
