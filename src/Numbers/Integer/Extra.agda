module Numbers.Integer.Extra where

open import Foundation 
open import Data.Fin as Fin using (Fin)
open import Data.Fin.Patterns
open import Data.Integer
open import Data.Integer.Properties
import Numbers.Nat as ℕ
open import Numbers.Integer.Congruence
open import Numbers.Integer.DivMod
open import Numbers.Integer.Literals
open import Relation.Nullary.Decidable

infixl 9 _^_

ℕ→ℤ : ℕ → ℤ
ℕ→ℤ = +_

----------------------------------------------------------------------
-- properties of ∣_∣ and _≤_

i≤∣i∣ : ∀ i → i ≤ ℕ→ℤ ∣ i ∣
i≤∣i∣ (+ i) = ≤-refl
i≤∣i∣ -[1+ i ] = -≤+

----------------------------------------------------------------------
-- parity

Even Odd : ℤ → Set
Even i = True (i ≟ 0 [mod 2 ])
Odd i = True (i ≟ 1 [mod 2 ])

----------------------------------------------------------------------
-- exponentiation

_^_ : ℤ → ℕ → ℤ
i ^ 0  = 1
i ^ ℕ.suc n = i * i ^ n

_² : ℤ → ℤ
i ² = i * i
