module Numbers.Integer.Literals where

open import Data.Unit using (tt) public
open import Data.Integer.Literals
open import Agda.Builtin.FromNat using (fromNat) public
open import Agda.Builtin.FromNeg using (fromNeg) public
instance
  Numberℤ = number
  Negativeℤ = negative
