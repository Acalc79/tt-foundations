module Numbers.Integer.DivMod where

open import Data.Fin as Fin using (Fin)
open import Data.Integer
open import Data.Integer.DivMod public
open import Foundation
import Numbers.Nat as ℕ

----------------------------------------------------------------------
-- modFin

infixl 7 _modFin_
_modFin_ : (n : ℤ)(d : ℕ){≢0 : False (d ℕ.≟ 0)} → Fin d
(n modFin d) {d≢0} = Fin.fromℕ< (n%ℕd<d n d {d≢0})


