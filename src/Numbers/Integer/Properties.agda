module Numbers.Integer.Properties where

open import Foundation

open import Data.Integer
open import Data.Integer.Divisibility.Signed
open import Data.Integer.Properties
import Data.Integer.Literals as ℤ
instance _ = ℤ.number

private variable i j k : ℤ

------------------------------------------------------------------------
-- Properties of _+_ and _<_
  
<-steps : NonNegative i → j < k → j < i + k
<-steps {i}{j}{k} i≥0 j<k =
  subst (_< i + k) (+-identityˡ j) $
  +-mono-≤-< {0}{i}{j}{k} (nonNegative⁻¹ i≥0) j<k

i<i+j : ∀{i j} → Positive j → i < i + j
i<i+j {i}{j} j>0 =
  subst (_< i + j) (+-identityʳ i) $
  +-monoʳ-< i (positive⁻¹ {j} j>0)

i<j+i : ∀{i j} → Positive j → i < j + i
i<j+i {i}{j} j>0 rewrite +-comm j i = i<i+j {i}{j} j>0

------------------------------------------------------------------------
-- Properties of _∣_

infix 4 _∣0 1∣_

_∣0 : ∀ i  → i ∣ 0
i ∣0 = divides 0 refl

1∣_ : ∀ i  → 1 ∣ i
1∣ i = divides i $ sym $ *-identityʳ i
