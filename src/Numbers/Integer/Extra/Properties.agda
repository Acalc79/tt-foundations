module Numbers.Integer.Extra.Properties where

import Data.Fin as Fin
open import Data.Fin.Patterns
open import Data.Integer
open import Data.Integer.Properties
open import Foundation
open import Numbers.Integer.Congruence
open import Numbers.Integer.DivMod
open import Numbers.Integer.Extra
open import Numbers.Integer.Literals
open import Relation.Unary

private variable
  i j k : ℤ

even∨odd : ∀ i → Even i ⊎ Odd i
even∨odd i with i modFin 2 | ≡ₘmodFin {2} i
... | 0F | i≡0[mod2] = inj₁ (fromWitness i≡0[mod2])
... | 1F | i≡1[mod2] = inj₂ (fromWitness i≡1[mod2])

even⇒¬odd : ∀ i → Even i → ¬ Odd i
even⇒¬odd i even odd with () ←
  ≡ₘ⇒≡ {1}{0} _ _ (from-yes (1 <? 2))(from-yes (0 <? 2)) $
  ≡ₘ-trans (≡ₘ-sym $ toWitness odd) (toWitness even)

odd⇒¬even : ∀ i → Odd i → ¬ Even i
odd⇒¬even i = flip (even⇒¬odd i)

even? : Decidable Even
even? i = case even∨odd i of λ
  { (inj₁ even-i) → yes even-i
  ; (inj₂ odd-i) → no (odd⇒¬even i odd-i)}

odd? : Decidable Odd
odd? i = case even∨odd i of λ
  { (inj₁ even-i) → no (even⇒¬odd i even-i)
  ; (inj₂ odd-i) → yes odd-i}

