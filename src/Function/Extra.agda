module Function.Extra where

open import Data.Product
open import Level
open import Relation.Binary.PropositionalEquality
open import Set

private variable
  a b l : Level
  A B : Set a

image : {A : Set a}{B : Set b}(f : A → B) → ℙ′ (a ⊔ b) B
image {A = A} f b = ∃[ a ] f a ≡ b
