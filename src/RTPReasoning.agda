module RTPReasoning where

open import Level
open import Function.Reasoning
private variable
  a b : Level
  A B : Set a

infixr 0 goal_from_ RTP-syntax

goal_from_ : (A : Set a)(x : A) → A
goal_from_ from: x = x
{-# INLINE goal_from_ #-}

-- RTP-syntax : (A : Set a){B : A → Set b}(f : (x : A) → B x)(x : A) → B x
-- RTP-syntax _ f x = f x
-- {-# INLINE RTP-syntax #-}

-- syntax RTP-syntax A f x = f RTP′  A from x

RTP-syntax : (A : Set a){B : Set b}(f : A → B)(x : A) → B
RTP-syntax _ f x = f x
{-# INLINE RTP-syntax #-}

syntax RTP-syntax A f x = f RTP: A from x
