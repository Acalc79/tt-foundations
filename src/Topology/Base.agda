module Topology.Base where

open import Level
open import Relation.Unary
open import Function
open import Data.Nat using (ℕ)
open import Data.Fin using (Fin)

1ℓ : Level
1ℓ = suc 0ℓ

ℙ : ∀ {a} → Set a → Set (a ⊔ 1ℓ)
ℙ A = Pred A 0ℓ

_⃖_ : ∀{a b}{X : Set a}{Y : Set b}(f : X → Y)(U : ℙ Y) → ℙ X
(f ⃖ U) x = f x ∈ U

-- Pred A 0ℓ = 𝒫 A
-- a : Pred A 0ℓ = a ⊆ A
-- U ⊆ 𝒫 X = U : Pred (Pred X 0ℓ) 0ℓ

record is-topology (X : Set)(𝒰 : ℙ (ℙ X)) : Set₁ where
  field
    ∅∈𝒰 : ∅ ∈ 𝒰
    X∈𝒰 : U ∈ 𝒰
    ⋃∈𝒰 : {A : Set}(V : A → ℙ X) →
      (∀ α → V α ∈ 𝒰) →
      ⋃[ α ∶ A ] V α ∈ 𝒰
    fin⋂∈𝒰 : {n : ℕ}(V : Fin n → ℙ X) →
      (∀ i → V i ∈ 𝒰) →
      ⋃[ i ∶ Fin n ] V i ∈ 𝒰

record TopologicalSpace : Set₂ where
  field
    X : Set
    𝒰 : ℙ (ℙ X)
    topology : is-topology X 𝒰
    
  open-sets : ℙ (ℙ X)
  open-sets = 𝒰
  _is-open : (U : ℙ X) → Set
  U is-open = U ∈ 𝒰

  open is-topology topology public

is-open-syntax :
  (T : TopologicalSpace)
  (U : ℙ (TopologicalSpace.X T))
  → ------------------------------
  Set
is-open-syntax T U = U is-open
  where open TopologicalSpace T

syntax is-open-syntax T U = U is-open-in T

open import Data.Product
open import Relation.Binary.PropositionalEquality

module DefContinuous ⦃ X-top Y-top : TopologicalSpace ⦄ where
  module X = TopologicalSpace X-top
  module Y = TopologicalSpace Y-top
  open X using (X)
  open Y renaming (X to Y) using ()
  
  _is-continuous : (f : X → Y) → Set₁
  f is-continuous = {U : ℙ Y} → U Y.is-open → (f ⃖ U) is-open-in X-top

open DefContinuous using (_is-continuous)

module _ ⦃ X-top Y-top Z-top : TopologicalSpace ⦄ where
  private
    module X = TopologicalSpace X-top
    module Y = TopologicalSpace Y-top
    module Z = TopologicalSpace Z-top
    -- module XY = DefContinuous X-top Y-top
    -- module YZ = DefContinuous Y-top Z-top
    -- module XZ = DefContinuous X-top Z-top
  open X using (X)
  open Y renaming (X to Y) using ()
  open Z renaming (X to Z) using ()
  open DefContinuous
  
  ∘-continuous : {f : X → Y}{g : Y → Z}
    (f-cont : f is-continuous)(g-cont : g is-continuous)
    → ------------------------------------------------------------
    (g ∘ f) is-continuous
  ∘-continuous f-cont g-cont = f-cont ∘ g-cont

module DefHomeomorphism ⦃ X-top Y-top : TopologicalSpace ⦄ where
  module X = TopologicalSpace X-top
  module Y = TopologicalSpace Y-top
  open X using (X)
  open Y renaming (X to Y) using ()
  
  _is-homeomorphism : (ff⁻¹ : (X → Y) × (Y → X)) → Set₁
  (f , f⁻¹) is-homeomorphism =
    Inverseᵇ _≡_ _≡_ f f⁻¹ ×
    f is-continuous × f⁻¹ is-continuous

open DefHomeomorphism public

homeomorphic _≃_ : (X-top Y-top : TopologicalSpace) → Set₁
homeomorphic X-top Y-top =
  Σ[ ff⁻¹ ∈ (X → Y) × (Y → X) ] ff⁻¹ is-homeomorphism
  where open TopologicalSpace X-top using (X)
        open TopologicalSpace Y-top renaming (X to Y) using ()
        instance
          _ = X-top
          _ = Y-top
_≃_ = homeomorphic
