module Data.Vec.Heterogeneous.Base where

open import Data.Product.Nary.NonDependent
open import Foundation
import Extra.Data.Product.Nary.NonDependent
import Extra.Function.Nary.NonDependent as F
import Function.Nary.NonDependent as F
open import Numbers.Nat hiding (_⊔_)

private variable
  n : ℕ

------------------------------------------------------------------------
-- Levels

Levels : (n : ℕ) → Set _
Levels n = Product⊤ n (F.sreplicate n Level)

⨆ : Levels n → Level
⨆ = {!!} -- V.foldr (const Level) _⊔_ 0ℓ

{-
------------------------------------------------------------------------
-- Sets

private variable
  a : Level
  A : Set a
  ls : Levels n

toFLevels : Levels n → F.Levels n
toFLevels = V.foldr F.Levels _,_ _

Levels-toFSets : (ls : Levels n) → F.Sets n (F.lmap lsuc n (as-FLevels ls))
Levels-toFSets V.[] = _
Levels-toFSets (l V.∷ ls) = Set l , as-FSets ls

Sets : (ls : Levels n) → Set _
Sets {n} ls = Product⊤ n (toFSets ls)

-- toFSets : Sets ls → F.Sets n ()

Vec : ∀{n}{ls : Levels n} → Sets ls → Set (⨆ ls)
Vec {n}{ls} As = Product⊤ n (as-FSets ls)
-}
