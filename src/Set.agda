module Set where

open import Data.Product
open import Function using (_⇔_)
open import Level
open import Relation.Binary.PropositionalEquality
open import Relation.Unary

private variable
  a b c d l l₁ l₂ : Level
  A : Set a

1ℓ : Level
1ℓ = suc 0ℓ

ℙ : Set a → Set (a ⊔ 1ℓ)
ℙ A = Pred A 0ℓ

ℙ′ : ∀ b → Set a → Set _
ℙ′ b A = Pred A b

rep-syntax : {A : Set a}{B : Set b}(f : A → B) → ℙ′ l A → ℙ′ _ B
rep-syntax f S b = ∃[ a ] a ∈ S × f a ≡ b
{-# INLINE rep-syntax #-}

syntax rep-syntax (λ a → b) S = ｛ b ∣ a ∈ S ｝

sep-syntax : (P : ℙ′ l A) → ℙ′ l A
sep-syntax P = P
{-# INLINE sep-syntax #-}

syntax sep-syntax (λ a → P) = ｛ a ∣ P ｝

∀-∈-syntax : {A : Set a} → ℙ′ l₁ A → ℙ′ l₂ A → Set _
∀-∈-syntax = _⊆_

syntax ∀-∈-syntax A (λ a → P) = ∀[ a ∈ A ] P

∃-∈-syntax : {A : Set a} → ℙ′ l₁ A → ℙ′ l₂ A → Set _
∃-∈-syntax = _≬_

syntax ∃-∈-syntax A (λ a → P) = ∃[ a ∈ A ] P

Inhabited = Satisfiable

_≅_ : {A : Set a}(S₁ S₂ : ℙ A) → Set a
S₁ ≅ S₂ = ∀ x → x ∈ S₁ ⇔ x ∈ S₂
