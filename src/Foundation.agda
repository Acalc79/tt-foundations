module Foundation where

open import Level public
  renaming (suc to lsuc) hiding (zero)
open import Data.Empty public
open import Data.Product public
  hiding (assocʳ; assocˡ; map; map₁; map₂; swap)
open import Data.Sum public
  hiding (assocʳ; assocˡ; map; map₁; map₂; swap)
open import Data.Unit public
  using (⊤; tt)
open import Function public
open import Function.Reasoning public
open import Numbers.Nat as ℕ public
  using (ℕ)
open import Numbers.Nat.Literals public
open import Relation.Nullary public
open import Relation.Nullary.Negation public
open import Relation.Nullary.Decidable public
  hiding (map)
open import Relation.Unary public
  hiding (_⇒_; _//_; ⌊_⌋; Decidable; Irrelevant; Recomputable)
open import Relation.Binary public
  hiding (_⇒_; _⇔_; Decidable; Irrelevant; Recomputable; Universal)
open import Relation.Binary.PropositionalEquality
  hiding ([_]) public
open import RTPReasoning public
open import Set public

module Bin = Relation.Binary
module DEC = Relation.Nullary.Decidable
module Prod = Data.Product
module Sum = Data.Sum
module Un = Relation.Unary
module Unit = Data.Unit
